<?php

namespace App\View\Components;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\View\Component;

class Sidebar extends Component
{
    public function __construct()
    {
        //
    }

    public function render()
    {
        $menus = Menu::index();
        // $segment = Request::segments();
        // $uri = route();
        // dd($menus);

        // return view('components.sidebar', ['menus' => $menus]);
        return view('components.sidebar', compact('menus'));
    }
}
