<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Hello extends Component
{
    public $message;
    

    public function mount()
    {
        $this->message = 'Hello World!';
    }

    public function render()
    {
        return view('hello');
    }

   
}
