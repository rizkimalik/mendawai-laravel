<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\AccessMenu;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    public function menu()
    {
        // $menu = Modul::all();
        $menu = Menu::index();
        return $menu;
    }
    
    public function access()
    {
        $menu = AccessMenu::index();

        return $menu;
    }
}
