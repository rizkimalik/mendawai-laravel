<?php

namespace App\Http\Controllers\ManagementUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserPrivilegeController extends Controller
{
    public function index()
    {
        $users = User::orderBy('name')->get();

        return view('management-user.privilege', ['users' => $users]);
    }

    public function show(User $user)
    {
        return view('management-user.detail-user', ['user' => $user]);
    }

  
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(User $user)
    {
        $test = User::where('userid',$user->userid)->delete();

        return back();
    }

}
