<?php

namespace App\Http\Controllers\ManagementUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Menu;
use App\Models\Modul;
use App\Models\SubModul;
use App\Models\AccessMenu;
use Illuminate\Support\Facades\DB;

class MenuAccessController extends Controller
{
    public function index()
    {
        $menus = AccessMenu::all();

        return view('management-user.menu-access', ['menus' => $menus]);
    }


    public function show(User $user)
    {
        $menus = DB::table('view_menu')->get();

        // return $menus;

        return view('management-user.menu-access', [
            'menus' => $menus, 
            'userid' => $user->username
        ]);
    }

    public function show2(User $user)
    {
        $menus = Menu::orderBy('Number')->get();
        
        $menus_arr = [];
        foreach ($menus as $menu) {
            $moduls = Modul::where('MenuID', $menu->MenuID)->get();

            $moduls_arr = [];
            foreach ($moduls as $modul) {
                $submoduls = SubModul::where('SubMenuID', $modul->SubMenuID)->get();

                $submoduls_arr = [];
                foreach ($submoduls as $submodul) {
                    //? Data SubModulMenu
                    $access_data_submodul = AccessMenu::select('MenuIDTree')
                        ->where('MenuIDTree', $modul->SubMenuIDTree)
                        ->where('UserID', $user->username)
                        ->groupBy('MenuIDTree')
                        ->first();

                    // $access_submodul = $access_data_submodul->MenuIDTree == 'null' ? false : true;
                    $access_submodul =  true;
                    $submoduls_arr[] = [
                        "SubMenuIDTree" => $submodul->SubMenuIDTree,
                        "MenuTreeName" => $submodul->MenuTreeName,
                        "Url" => $submodul->Url,
                        "Access" => $access_submodul,
                    ];
                }


                //? Data Modul
                $access_data_modul = AccessMenu::select('SubMenuID')
                    ->where('SubMenuID', $menu->SubMenuID)
                    ->where('UserID', $user->username)
                    ->groupBy('SubMenuID')
                    ->first();
            
                // $access_modul = $access_data_modul->SubMenuID == 'null' ? false : true;
                $access_modul = count($access_data_modul->SubMenuID);
                $modul_tree = count($submoduls) > 0 ? true : false;
                $moduls_arr[] = [
                    "SubMenuID" => $modul->SubMenuID,
                    "SubMenuName" => $modul->SubMenuName,
                    "Url" => $modul->Url,
                    "Access" => $access_modul,
                    "modul_tree" => $modul_tree,
                    "submodul" => $submoduls_arr
                ];
            }


            //? Data Menu
            $access_data = AccessMenu::select('MenuID')
                ->where('MenuID', $menu->MenuID)
                ->where('UserID', $user->username)
                ->groupBy('MenuID')
                ->first();
            
            $access_menu = $access_data == null ? false : true;
            $menu_tree = count($moduls) > 0 ? true : false;
            $menus_arr[] = [
                "MenuID" => $menu->MenuID,
                "MenuName" => $menu->MenuName,
                "Url" => $menu->Url,
                "Access" => $access_menu,
                "menu_tree" => $menu_tree,
                "modul" => $moduls_arr
            ]; 
        }

        // return $menus_arr;

        return view('management-user.menu-access', [
            'menus' => $menus_arr, 
            'userid' => $user->username
        ]);
    }

    public function store(Request $request, User $user)
    {
        AccessMenu::where('UserID', $user->username)->delete();

        $data = $request->except('_token');
        $access_menu = AccessMenu::insert($data);
       
        return $access_menu;

    }

    public function update(Request $request, $id)
    {
        //
    }



}
