<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AccessMenu extends Model
{
    use HasFactory;

    protected $table = 'menu_access';
    
    protected $primaryKey = 'ID';

    public $timestamps = true;

    /* public static function index()
    {
        $user = Auth::User();
        $menus = Menu::orderBy('Number')->get();
        
        $menus_arr = [];
        foreach ($menus as $menu) {
            $moduls = Modul::where('MenuID', $menu->MenuID)->get();

            $moduls_arr = [];
            foreach ($moduls as $modul) {
                $submoduls = SubModul::where('SubMenuID', $modul->SubMenuID)->get();

                $submoduls_arr = [];
                foreach ($submoduls as $submodul) {
                    //? Data SubModulMenu
                    $access_data_submodul = AccessMenu::select('MenuIDTree')
                        ->where('MenuIDTree', $menu->SubMenuIDTree)
                        ->where('UserID', $user->username)
                        ->groupBy('MenuIDTree')
                        ->first();

                    $access_submodul = $access_data_submodul == null ? false : true;
                    $submoduls_arr[] = [
                        "SubMenuIDTree" => $submodul->SubMenuIDTree,
                        "MenuTreeName" => $submodul->MenuTreeName,
                        "Url" => $submodul->Url,
                        "Access" => $access_submodul,
                    ];
                }


                //? Data Modul
                $access_data_modul = AccessMenu::select('SubMenuID')
                    ->where('SubMenuID', $menu->SubMenuID)
                    ->where('UserID', $user->username)
                    ->groupBy('SubMenuID')
                    ->first();
            
                $access_modul = $access_data_modul == null ? false : true;
                $modul_tree = count($submoduls) > 0 ? true : false;
                $moduls_arr[] = [
                    "SubMenuID" => $modul->SubMenuID,
                    "SubMenuName" => $modul->SubMenuName,
                    "Url" => $modul->Url,
                    "Access" => $access_modul,
                    "modul_tree" => $modul_tree,
                    "submodul" => $submoduls_arr
                ];
            }


            //? Data Menu
            $access_data = AccessMenu::select('MenuID')
                ->where('MenuID', $menu->MenuID)
                ->where('UserID', $user->username)
                ->groupBy('MenuID')
                ->first();
            
            $access_menu = $access_data == null ? false : true;
            $menu_tree = count($moduls) > 0 ? true : false;
            $menus_arr[] = [
                "MenuID" => $menu->MenuID,
                "MenuName" => $menu->MenuName,
                "Url" => $menu->Url,
                "Access" => $access_menu,
                "menu_tree" => $menu_tree,
                "modul" => $moduls_arr
            ]; 
        }

        return $menus_arr;
    } */

}
