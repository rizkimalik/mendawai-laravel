<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Menu extends Model
{
    use HasFactory;

    protected $table = 'menu1';
    protected $primaryKey = 'MenuID';

    public static function index()
    {
        $users = Auth::User();
        // dd($users->username);
        //! kurang ke tbl akses

        $menus = Menu::join('menu_access', 'menu1.MenuID', '=', 'menu_access.MenuID')
            ->where('menu_access.UserID', $users->username)
            ->select('menu1.Number', 'menu1.MenuID', 'menu1.MenuName', 'menu1.Url', 'menu1.Icon')
            ->groupBy('menu1.Number', 'menu1.MenuID', 'menu1.MenuName', 'menu1.Url', 'menu1.Icon')
            ->orderBy('Number')
            ->get();

        $menus_arr = [];
        foreach ($menus as $menu) {

            // $moduls = DB::table('menu2')->where('MenuID', '=', $menu->MenuID)->get();
            $moduls = Modul::where('MenuID', $menu->MenuID)->get();

            $moduls_arr = [];
            foreach ($moduls as $modul) {

                // $submoduls = DB::table('menu3')->where('SubMenuID', '=', $modul->SubMenuID)->get();
                $submoduls = SubModul::where('SubMenuID', $modul->SubMenuID)->get();

                $modul_tree = count($submoduls) > 0 ? true : false;
                $moduls_arr[] = [
                    "SubMenuID" => $modul->SubMenuID,
                    "SubMenuName" => $modul->SubMenuName,
                    "Url" => $modul->Url,
                    "modul_tree" => $modul_tree,
                    "submodul" => $submoduls
                ];
            }

            $menu_tree = count($moduls) > 0 ? true : false;
            $menus_arr[] = [
                "MenuID" => $menu->MenuID,
                "MenuName" => $menu->MenuName,
                "Url" => $menu->Url,
                "Icon" => $menu->Icon,
                "menu_tree" => $menu_tree,
                "modul" => $moduls_arr
            ];
        }

        // return response()->json($menus_arr);
        return $menus_arr;

        // return response()->json([
        //     "status" => "success",
        //     "data"  => ["menu"=>$menu, "sub"=>$menu_sub]
        // ]);

        // return $menu;
    }

}
