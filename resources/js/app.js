// require('./bootstrap');

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import Alpine from 'alpinejs'
window.Alpine = Alpine
Alpine.start();

// var Turbolinks = require("turbolinks")
// Turbolinks.start();