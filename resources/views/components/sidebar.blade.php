@php
 	$segment = Request::segments();
	$uri = Route::current()->uri;   
@endphp



<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
	<!--begin::Brand-->
	<div class="brand flex-column-auto" id="kt_brand">
		<a href="{{ url('/') }}" class="brand-logo">
			<img height="45px;" alt="Logo" src="{{ asset('assets/media/logos/logo-hitam-50.png') }}" />
		</a>
		<button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
			<span class="svg-icon svg-icon svg-icon-xl">
                <x-icons.aside />
			</span>
		</button>
	</div>
	<!--end::Brand-->

	<!--begin::Aside Menu-->
	<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
		<!--begin::Menu Container-->
		<div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
			
			<!--begin::Menu Nav-->
			<ul class="menu-nav">
		
				@foreach ($menus as $menu)
					<li class="menu-item menu-item-submenu  @if($segment[0] == $menu['Url']) menu-item-active menu-item-open menu-item-here @endif" aria-haspopup="true" data-menu-toggle="hover">
						<a href="{{ url($menu['Url']) }}" class="menu-link menu-toggle">
							<span class="svg-icon menu-icon">
								@include("components.icons.".$menu['Icon'])
							</span>
							<span class="menu-text">{{ $menu['MenuName'] }}</span>

							@if ($menu['menu_tree'])
							<i class="menu-arrow"></i>
							@endif
						</a>

						@if ($menu['menu_tree'])
						<div class="menu-submenu">
							<i class="menu-arrow"></i>
							<ul class="menu-subnav">

								@foreach ($menu['modul'] as $modul)

									<li class="menu-item @if($uri == $menu['Url'].'/'.$modul['Url']) menu-item-active @endif" aria-haspopup="true" data-menu-toggle="hover">
										<a href="{{ url($menu['Url'].'/'.$modul['Url']) }}" class="menu-link menu-toggle">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">{{ $modul['SubMenuName'] }}</span>

											@if ($modul['modul_tree'])
											<i class="menu-arrow"></i>
											@endif
										</a>

										@if ($modul['modul_tree'] == true)
											<div class="menu-submenu" kt-hidden-height="240" style="display: none; overflow: hidden;">
												<i class="menu-arrow"></i>
												<ul class="menu-subnav">

													@foreach ($modul['submodul'] as $submodul)
														{{-- <li class="menu-item @if($uri == $menu['Url'].'/'.$modul['Url'].'/'.$submodul['Url']) menu-item-active @endif" aria-haspopup="true"> --}}
														<li class="menu-item" aria-haspopup="true">
															<a href="?page=email" class="menu-link">
																<i class="menu-bullet menu-bullet-line">
																	<span></span>
																</i>
																<span class="menu-text">{{ $submodul->MenuTreeName }}</span>
															</a>
														</li>
													@endforeach

												</ul>
											</div>
										@endif
									</li>
								@endforeach

							</ul>
						</div>
						@endif
					</li>
				@endforeach

				<li class="menu-item" aria-haspopup="true">
					<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
						@csrf
					</form>
					<a href="{{ route('logout') }}" class="menu-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
						<span class="svg-icon menu-icon">
							@include("components.icons.sign-out")
						</span>
						<span class="menu-text">Exit</span>
					</a>
				</li>

			</ul>
			<!--end::Menu Nav-->
		</div>
		<!--end::Menu Container-->
	</div>
	<!--end::Aside Menu-->
</div>