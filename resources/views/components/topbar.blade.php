<div id="kt_header" class="header header-fixed">
    <!--begin::Container-->
    <div class="container-fluid d-flex align-items-stretch justify-content-between">

        <!--begin::Header Menu Wrapper-->
        <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">

            <!--begin::Header Menu-->
            <div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">

                <ul class="menu-nav">

                    <li class="menu-item " data-menu-toggle="click" aria-haspopup="true">
                        <a href="javascript:;" class="menu-link">
                            <span class="svg-icon menu-icon">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Safe-chat.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <path d="M8,17 C8.55228475,17 9,17.4477153 9,18 L9,21 C9,21.5522847 8.55228475,22 8,22 L3,22 C2.44771525,22 2,21.5522847 2,21 L2,18 C2,17.4477153 2.44771525,17 3,17 L3,16.5 C3,15.1192881 4.11928813,14 5.5,14 C6.88071187,14 8,15.1192881 8,16.5 L8,17 Z M5.5,15 C4.67157288,15 4,15.6715729 4,16.5 L4,17 L7,17 L7,16.5 C7,15.6715729 6.32842712,15 5.5,15 Z" fill="#000000" opacity="0.3"></path>
                                        <path d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z" fill="#000000"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <span class="menu-text">Sosial Media</span>
                        </a>
                    </li>

                </ul>

            </div>

            <!--end::Header Menu-->
        </div>

        <!--end::Header Menu Wrapper-->

        <!--begin::Topbar-->
        <div class="topbar">

            <!--end::Notifications-->

            <!--begin::Quick Panel-->
            <div id="kt_quick_panel" class="offcanvas offcanvas-right pt-5 pb-10">
                <!--begin::Header-->
                <div class="offcanvas-header offcanvas-header-navs d-flex align-items-center justify-content-between mb-5">
                    <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-primary flex-grow-1 px-10" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_sugestions">Suggestions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_knowledge">Knowledge</a>
                        </li>
                    </ul>
                    <div class="offcanvas-close mt-n1 pr-5">
                        <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_panel_close">
                            <i class="ki ki-close icon-xs text-muted"></i>
                        </a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Content-->
                <div class="offcanvas-content px-10">
                    <div class="tab-content">
                        <!--begin::Tabpane-->
                        <div class="tab-pane fade show pt-3 pr-5 mr-n5 active" id="kt_quick_panel_sugestions" role="tabpanel">
                            <!--begin::Section-->
                            <div class="mb-15">
                                <h5 class="font-weight-bold mb-5">System Sugestions</h5>

                                <!--begin: Item-->
                                <div class="d-flex align-items-center flex-wrap mb-5">
                                    <div class="symbol symbol-50 symbol-light mr-5">
                                        <span class="symbol-label">
                                            <img src="{{ asset('assets/media/svg/misc/015-telegram.svg') }}" class="h-50 align-self-center" alt="" />
                                        </span>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1 mr-2">
                                        <a href="#" class="font-weight-bolder text-dark-75 text-hover-primary font-size-lg mb-1">093811/Penutup</a>
                                        <span class="text-muted font-weight-bold">( 18% )</span>
                                    </div>
                                    <span class="btn btn-sm btn-light font-weight-bolder my-lg-0 my-2 py-1 text-dark-50">Suggest</span>
                                </div>
                                <!--end: Item-->

                                <!--begin: Item-->
                                <div class="d-flex align-items-center flex-wrap mb-5">
                                    <div class="symbol symbol-50 symbol-light mr-5">
                                        <span class="symbol-label">
                                            <img src="{{ asset('assets/media/svg/misc/015-telegram.svg') }}" class="h-50 align-self-center" alt="" />
                                        </span>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1 mr-2">
                                        <a href="#" class="font-weight-bolder text-dark-75 text-hover-primary font-size-lg mb-1">093811/Terimakasih</a>
                                        <span class="text-muted font-weight-bold">( 18% )</span>
                                    </div>
                                    <span class="btn btn-sm btn-light font-weight-bolder my-lg-0 my-2 py-1 text-dark-50">Suggest</span>
                                </div>
                                <!--end: Item-->
                                <form class="form">
                                    <div>

                                        <div class="form-group">
                                            <label>Chat:</label>
                                            <textarea type="email" class="form-control form-control-solid" placeholder="Bagaimana Kawan Penutupnya" readonly></textarea>
                                            <span class="form-text text-muted">We'll never share your email with
                                                anyone else</span>
                                        </div>

                                    </div>
                                    <div class="card-footer">
                                        <button type="reset" class="btn btn-primary mr-2">Submit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </form>
                            </div>
                            <!--end::Section-->

                        </div>
                        <!--end::Tabpane-->

                        <!--begin::Tabpane-->
                        <div class="tab-pane fade pt-2 pr-5 mr-n5" id="kt_quick_panel_knowledge" role="tabpanel">
                            <!--begin::Nav-->
                            Knowledge
                            <!--end::Nav-->
                        </div>
                        <!--end::Tabpane-->

                    </div>
                </div>
                <!--end::Content-->
            </div>
            <!--end::Quick Panel-->

            <!--begin::User-->
            <div class="topbar-item">
                <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                    <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                    <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ Auth::user()->name }}</span>
                    <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
                        <span class="symbol-label font-size-h5 font-weight-bold">A</span>
                    </span>
                </div>
                
            </div>
            
            <!--end::User-->
        </div>

        <!--end::Topbar-->
    </div> 
    <!--end::Container-->

    
</div>