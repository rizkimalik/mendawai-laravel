<x-app-layout>
    <!--begin::Row-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin::Charts Widget 6-->
            <div class="card card-custom card-stretch gutter-b">
                <!--begin::Header-->
                <div class="card-header h-auto border-0">
                    <div class="card-title py-5">
                        <h3 class="card-label">
                            <span class="d-block text-dark font-weight-bolder">Statistik Seluruh Channel</span>
                            <span class="d-block text-muted mt-2 font-size-sm">Data laporan keluhan dari seluruh channel.</span>
                            
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <span class="mr-5 d-flex align-items-center font-weight-bold">
                            <i class="label label-dot label-xl label-primary mr-2"></i>Realtime Data</span>

                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    setting
                </div>
                <!--end::Body-->
            </div>
            <!--end::Charts Widget 6-->
        </div>
    </div>
    <!--end::Row-->
</x-app-layout>