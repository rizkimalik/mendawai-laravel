<x-guest-layout>
    <div class="login-signin">
        <div class="mb-20">
            <h3>Sign In</h3>
            <div class="text-muted font-weight-bold">Enter your details to login to your account:</div>
        </div>

        <form class="form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group mb-5">
                <input class="form-control h-auto form-control-solid py-4 px-8 @error('username') is-invalid @enderror" type="text" placeholder="Username" name="username" id="username" value="{{ old('username') ?: old('email') }}" required autocomplete="username" autofocus />
                
                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group mb-5">
                <input class="form-control h-auto form-control-solid py-4 px-8 @error('password') is-invalid @enderror" type="password" placeholder="Password" name="password" id="password" required autocomplete="current-password" />

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                <div class="checkbox-inline">
                    <label class="checkbox m-0 text-muted">
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                        <span></span>Remember me</label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Sign In</button>
        </form>

        <div class="mt-10">
            <span class="opacity-70 mr-4">Don't have an account yet?</span>
            <a href="{{ route('register') }}" id="kt_login_signup" class="text-muted text-hover-primary font-weight-bold">Sign Up!</a>
        </div>
    </div>
</x-guest-layout>

