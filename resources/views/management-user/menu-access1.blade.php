<x-app-layout>
    @slot('style')
        <link href="{{ asset('assets/plugins/custom/jstree/jstree.bundle.css') }}" rel="stylesheet" type="text/css" />
    @endslot

    <div class="row">
        <div class="col-xl-12">
            <form>
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Menu Access User</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="access-menu" class="tree-demo"></div>
                    </div>
                    <div class="card-footer">
                        {{-- <button type="button" onclick="AccessMenuHandler()" class="btn btn-primary mr-2">Submit</button> --}}
                        <button type="button" id="btn-submit" class="btn btn-primary mr-2">Submit</button>
                        <button type="button" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            <form>
        </div>
    </div>

    @slot('script')
        <script src="{{ asset('assets/plugins/custom/jstree/jstree.bundle.js') }}"></script>
        <script>
            const menus = @json($menus);
            const userid = @json($userid);

            // console.log(menus);

            let menu_data = [];
            menus.map((menu) => {

                let modul_data = [];
                menu.modul.map((modul) => {

                    //Data SubModul
                    let submodul_data = [];
                    modul.submodul.map((submodul) => {
                        submodul_data.push({
                            id: `SubModul-${menu.MenuID}-${modul.SubMenuID}-${submodul.SubMenuIDTree}`,
                            text: submodul.MenuTreeName,
                            state: {
                                selected: submodul.Access,
                                opened: submodul.Access === true ? true : false,
                            },
                        })
                    })

                    //Data Modul
                    modul_data.push({
                        id: `Modul-${menu.MenuID}-${modul.SubMenuID}`,
                        text: modul.SubMenuName,
                        state: {
                            selected: modul.Access,
                            opened: modul.Access === true ? true : false,
                        },
                        children: submodul_data
                    })
                });

                //Data Menu
                menu_data.push({
                    id: `Menu-${menu.MenuID}`,
                    text: menu.MenuName,
                    state: {
                        selected: menu.Access,
                        opened: menu.Access === true ? true : false,
                    },
                    children: modul_data
                })
            })            

            $('#access-menu').jstree({
                "plugins": ["wholerow", "checkbox", "types"],
                "core": {
                    "check_callback": true,
                    "themes": {
                        "responsive": true
                    },
                    "data": menu_data
                },
                "checkbox" : {
					"three_state": false,
					"keep_selected_style" : true
				},
                "types": {
                    "default": {
                        "icon": "fa fa-folder text-warning"
                    },
                    "file": {
                        "icon": "fa fa-file  text-warning"
                    }
                },
            });

            async function AccessMenuHandler() {
                const data_access = $('#access-menu').jstree(true).get_selected();
                // console.log(data_access);
                const userid = @json($userid);

                let new_access = [];
                data_access.map((item) => {
                    if(item.split('-')[0] == 'Menu') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.split('-')[1],
                            SubMenuID: '',
                            MenuIDTree: '',
                        });
                    }
                    else if(item.split('-')[0] == 'Modul') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.split('-')[1],
                            SubMenuID: item.split('-')[2],
                            MenuIDTree: '',
                        });
                    }
                    else if(item.split('-')[0] == 'SubModul') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.split('-')[1],
                            SubMenuID: item.split('-')[2],
                            MenuIDTree: item.split('-')[3],
                        });
                    }
                });

                const response = await fetch(`/api/menu-access/${userid}`, {
                    method: 'POST',
                    body: JSON.stringify(new_access),
                    headers: {
                        'Content-Type': 'application/json'
                    },
                });
                const status = await response.json();

                console.log(status);
                
            }

            $("#btn-submit").click(function(e){
                e.preventDefault();

                const data_access = $('#access-menu').jstree(true).get_selected();

                let new_access = [];
                data_access.map((item) => {
                    if(item.split('-')[0] == 'Menu') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.split('-')[1],
                            SubMenuID: '',
                            MenuIDTree: '',
                        });
                    }
                    else if(item.split('-')[0] == 'Modul') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.split('-')[1],
                            SubMenuID: item.split('-')[2],
                            MenuIDTree: '',
                        });
                    }
                    else if(item.split('-')[0] == 'SubModul') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.split('-')[1],
                            SubMenuID: item.split('-')[2],
                            MenuIDTree: item.split('-')[3],
                        });
                    }
                });

                $.ajax({
                    type:'POST',
                    url: `/management-user/menu-access/${userid}`,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {...new_access}, //array to object
                    success:function(data){
                        console.log(data);

                    }
                });
            });

        </script>
    @endslot
</x-app-layout>

