<x-app-layout title="{{ $user->name }}">
    <div class="row">
        <div class="col-xl-12">
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            User Profile
                            <span class="d-block text-muted pt-2 font-size-sm">data editing action</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <a type="button" href="{{ url('/management-user/privilege') }}" class="btn btn-light-primary font-weight-bolder">Back</a>
                    </div>
                </div>
    
                <!--begin::Form-->
                <form class="form">
                    <div class="card-body">
                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">1. Customer Info:</h3>
                        <div class="mb-15">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-right">Full Name:</label>
                                <div class="col-lg-6">
                                    <input type="email" class="form-control" placeholder="Enter full name" value="{{ $user->name }}">
                                    <span class="form-text text-muted">Please enter your full name</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-right">Email address:</label>
                                <div class="col-lg-6">
                                    <input type="email" class="form-control" placeholder="Enter email">
                                    <span class="form-text text-muted">We'll never share your email with anyone else</span>
                                </div>
                            </div>
                        </div>
                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">2. Customer Account:</h3>
                        <div class="mb-3">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-right">Holder:</label>
                                <div class="col-lg-6">
                                    <input type="email" class="form-control" placeholder="Enter full name">
                                    <span class="form-text text-muted">Please enter your account holder</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-right">Contact</label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-chain"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Phone number">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-lg-3 col-form-label text-right">Communication:</label>
                                <div class="col-lg-6">
                                    <div class="checkbox-inline">
                                        <label class="checkbox">
                                        <input type="checkbox">
                                        <span></span>Email</label>
                                        <label class="checkbox">
                                        <input type="checkbox">
                                        <span></span>SMS</label>
                                        <label class="checkbox">
                                        <input type="checkbox">
                                        <span></span>Phone</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <button type="reset" class="btn btn-success mr-2">Submit</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
</x-app-layout>