<x-app-layout>
    @slot('style')
    {{-- <link href="{{ asset('assets/plugins/custom/jstree/jstree.bundle.css') }}" rel="stylesheet" type="text/css" />
    --}}
    <link href="{{ asset('assets/plugins/devex/dx.material.blue.light.compact.css') }}" rel="stylesheet"
        type="text/css" />
    @endslot

    <div class="row">
        <div class="col-xl-12">
            <form>
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Menu Access User</h3>
                        </div>
                    </div>
                    <div class="card-body">

                        {{-- <div id="access-menu" class="tree-demo"></div> --}}
                        <div id="access-menu"></div>

                    </div>
                    <div class="card-footer">
                        <button type="button" id="btn-submit" class="btn btn-primary mr-2">Submit</button>
                        <button type="button" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @slot('script')
    {{-- <script src="{{ asset('assets/plugins/custom/jstree/jstree.bundle.js') }}"></script> --}}
    <script src="{{ asset('assets/plugins/devex/dx.all.js') }}"></script>

    <script>
        const menus = @json($menus);
        const userid = @json($userid);


            const treeList = $("#access-menu").dxTreeList({
                dataSource: menus,
                keyExpr: "ID",
                parentIdExpr: "RootID",
                showRowLines: true,
                showBorders: true,
                showColumnLines: true,
                showRowLines: true,
                rowAlternationEnabled: false,
                hoverStateEnabled: true,
                columnAutoWidth: true,
                selection: {
                    mode: "multiple",
                    recursive: false,
                },
                // selectionFilter: ["State", "=", "California"],
                columns: [{ 
                        dataField: "MenuName",
                        caption: "Menu Name",
                        width: 350
                    }, 
                    {
                        dataField: "Url",
                        caption: "URL"
                    },
                    {
                        dataField: "Level",
                        caption: "Level",
                        width: 250,
                        cellTemplate: function (container, options) {
                            const Level = options.data.Level;
                            
                            if (Level == 'Menu') {
                                $(`<span class="label label-lg label-light-primary label-inline">${Level}</span>`)
                                .appendTo(container);
                            } 
                            else if (Level == 'Modul') {
                                $(`<span class="label label-lg label-light-warning label-inline">${Level}</span>`)
                                .appendTo(container);
                            }
                            else {
                                $(`<span class="label label-lg font-weight-boldlabel-light-primary label-inline">${Level}</span>`)
                                .appendTo(container);
                            }
                            
                        }
                    }
                ],
                autoExpandAll: true,
                onSelectionChanged: function(selectedItems) {
                    var selectedData = treeList.getSelectedRowsData("all");
                    // console.log(JSON.stringify(selectedData));
                    // $("#selected-items-container").text(getEmployeeNames(selectedData));
                }
            }).dxTreeList("instance");

            $("#btn-submit").click(function(e){
                e.preventDefault();
                const selectedData = treeList.getSelectedRowsData("all");
                // console.log(selectedData);

                let new_access = [];
                selectedData.map((item) => {
                    if(item.Level == 'Menu') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.MenuID,
                            SubMenuID: '',
                            MenuIDTree: '',
                        });
                    }
                    else if(item.Level == 'Modul') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.MenuID,
                            SubMenuID: item.SubMenuID,
                            MenuIDTree: '',
                        });
                    }
                    else if(item.Level == 'SubModul') {
                        new_access.push({
                            UserID: userid,
                            MenuID: item.MenuID,
                            SubMenuID: item.SubMenuID,
                            MenuIDTree: item.SubMenuIDTree,
                        });
                    }
                });
                console.log({...new_access});

                $.ajax({
                    type:'POST',
                    url: `/management-user/menu-access/${userid}`,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {...new_access}, //array to object
                    success:function(data){
                        console.log(data);

                    }
                });
            });


    </script>
    @endslot
</x-app-layout>