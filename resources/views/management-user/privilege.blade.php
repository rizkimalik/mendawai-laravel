<x-app-layout>
    @slot('style')
        <link href="{{ asset('assets/plugins/devex/dx.material.blue.light.compact.css') }}" rel="stylesheet" type="text/css"  />
    @endslot

    <!--begin::Row-->
    <div class="row">
        <div class="col-xl-12">
            <div class="card card-custom gutter-b kt_blockui_card">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label" >
                            User Setting Privilage 
                            <span class="d-block text-muted pt-2 font-size-sm">row selection user actions</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <div class="dropdown dropdown-inline mr-2">
                            <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Export</button>
                        </div>

                        <button type="button" id="btn_add" class="btn btn-primary font-weight-bolder mr-2">New Record</button>
                        <button type="button" id="btn_refresh" class="btn btn-primary font-weight-bolder mr-2">Refresh</button>
                    </div>
                </div>

                <div class="card-body mx-auto">
                    <div class="text-center" id="img_nodata">
                        <img src="{{ asset('assets/media/svg/undraw_web_development_w2vv.svg') }}" width="400px" alt="No Data"><br>
                        <p class="pt-5">No Data</p>
                    </div>
                    
                    <div class="demo-container">
                        <div id="tbl_user_privilege"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--end::Row-->

    @slot('script')
        <script src="{{ asset('assets/plugins/devex/dx.all.js') }}"></script>

        <script>
            document.addEventListener("DOMContentLoaded", () => {
               
                async function DataUserPrivilege() { 
                    const users = @json($users);
                    // console.log(user);
                    
                    $('#img_nodata').removeClass('hide');

                    if(users.length > 0){
                        $('#img_nodata').addClass('hide');
                        $("#tbl_user_privilege").dxDataGrid({
                            dataSource: users,
                            allowColumnReordering: true,
                            showBorders: true,
                            showColumnLines: true,
                            showRowLines: true,
                            rowAlternationEnabled: true,
                            hoverStateEnabled: true,
                            columnAutoWidth: true,
                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                visible: true,
                                allowedPageSizes: [5, 10, 20, 50, 100],
                                showPageSizeSelector: true,
                                showInfo: true,
                                showNavigationButtons: true
                            },
                            filterRow: {
                                visible: true,
                                applyFilter: "auto"
                            },
                            searchPanel: {
                                visible: true,
                                width: 240,
                                placeholder: "Search..."
                            },
                            keyExpr: "userid",
                            columns: [
                                {
                                    dataField: "name",
                                    caption: "Name",
                                },{
                                    dataField: "username",
                                    caption: "Username"
                                },{
                                    dataField: "email_address",
                                    caption: "Email User"
                                },{
                                    dataField: "leveluser",
                                    caption: "Level User"
                                },/* {
                                    dataField: "CHAT",
                                    caption: "CHAT",
                                    dataType: "boolean",
                                    calculateCellValue: function(rowData) {
                                        return rowData.CHAT == 1;
                                    }
                                },*/{
                                    dataField: "userid",
                                    caption: "Actions",
                                    allowFiltering: false,
                                    allowSorting: false,
                                    cellTemplate: function (container, options) {
                                        let username = options.data.username;
                                        let userid = options.data.userid;

                                        $("<div class='d-flex justify-content-center align-items-center'>")
                                            .append(`
                                                <a href="/management-user/privilege/${username}" class="btn btn-sm btn-warning btn-icon mx-2" title="Edit"><i class="la la-edit"></i></a>
                                                <a href="/management-user/menu-access/${username}" class="btn btn-sm btn-primary btn-icon mx-2" title="Setting"><i class="la la-gear"></i></a>
                                                <a href="/management-user/privilege/${userid}/delete" class="btn btn-sm btn-danger btn-icon mx-2" title="Delete"><i class="la la-trash"></i></a>
                                            `)
                                            .appendTo(container);
                                    }
                                    
                                },
                            ],
                            masterDetail: {
                                enabled: true,
                                template: function(container, options) { 
                                    // var currentEmployeeData = options.data;

                                    $("<div>")
                                        .addClass("master-detail-caption")
                                        .text("'s Tasks:")
                                        .appendTo(container);
                                }
                            },
                            /* editing: {
                                mode: "popup",
                                allowUpdating: true,
                                allowAdding: false,
                                allowDeleting: true,
                                popup: {
                                    title: "Employee Info",
                                    showTitle: true,
                                    width: 700,
                                    height: 525
                                },
                            }, */
                        }); 
 
                    }
                }
                
                DataUserPrivilege();

                $('#btn_refresh').click(function (e) { 
                    e.preventDefault();
                    DataUserPrivilege();
                });
            });
            
        </script>
    @endslot
</x-app-layout>