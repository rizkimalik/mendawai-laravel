<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ManagementUser\MenuAccessController;
use App\Http\Controllers\ManagementUser\UserPrivilegeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->group(function() {
    Route::view('/', 'auth.login');
    // Route::resource('/register', RegisterController::class)->only('create', 'store');  
});


Route::middleware(['auth', 'verified'])->group(function(){
    Route::resource('/home', HomeController::class)->only('index');   
    
     
    Route::view('/todolist', 'todolist'); 
    Route::view('/setting', 'setting'); 
    
    Route::prefix('management-user')->group(function () {
        Route::get('/privilege', [UserPrivilegeController::class, 'index']);    
        Route::get('/privilege/{user:username}', [UserPrivilegeController::class, 'show']); 
        Route::get('/privilege/{user:userid}/delete', [UserPrivilegeController::class, 'destroy']); 
        Route::get('/menu-access/{user:username}', [MenuAccessController::class, 'show']);    
        Route::post('/menu-access/{user:username}', [MenuAccessController::class, 'store']);    
        // Route::resource('/privilege', UserPrivilegeController::class)->only('index','edit','show');    
    });   
    
    
});




Route::get('/menu', 'HomeController@menu');  
// Route::get('/access', 'HomeController@access');  
