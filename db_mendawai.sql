﻿# Host: localhost  (Version 5.7.33)
# Date: 2021-10-21 22:21:31
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "failed_jobs"
#

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "failed_jobs"
#


#
# Structure for table "getsosmed_one"
#

DROP TABLE IF EXISTS `getsosmed_one`;
CREATE TABLE `getsosmed_one` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `P_ID_Post` varchar(50) DEFAULT NULL,
  `P_User_Name` varchar(50) DEFAULT NULL,
  `P_Message` longtext,
  `P_ddate` datetime DEFAULT NULL,
  `P_permalink_url` varchar(150) DEFAULT NULL,
  `P_ID_Page` varchar(50) DEFAULT NULL,
  `P_Channel` varchar(50) DEFAULT NULL COMMENT 'Facebook, Twitter,Instagram',
  `P_Filename` varchar(150) DEFAULT NULL,
  `P_Source` varchar(50) DEFAULT NULL,
  `P_Sosmed` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10047 DEFAULT CHARSET=latin1;

#
# Data for table "getsosmed_one"
#

INSERT INTO `getsosmed_one` VALUES (1,'374215903210349_827098514588750','Invision Astrindo Pratama','rumah','2021-05-26 03:04:39','https://www.facebook.com/InvisionAP/photos/a.488464041785534/827098514588750/?type=3','374215903210349','Facebook','374215903210349_827098514588750.jpg',NULL,NULL),(2,'109930817498267_265178311973516','Mendawai','architectural','2021-05-26 03:29:36','https://www.facebook.com/invision.mendawai/photos/a.265178328640181/265178311973516/?type=3','109930817498267','Facebook','109930817498267_265178311973516.jpg',NULL,NULL),(3,'109930817498267_265890908568923','Mendawai','Interior','2021-05-27 04:02:08','https://www.facebook.com/invision.mendawai/photos/a.265178328640181/265890908568923/?type=3','109930817498267','Facebook','109930817498267_265890908568923.jpg',NULL,NULL),(4,'17901316873953349','invisionapratama','Ini lingkaran','2021-05-28 10:53:52','https://www.instagram.com/p/CPSZHRHnrQm/','17841426077494261','Instagram','17901316873953349.jpg',NULL,NULL),(12,'17969463505401454','invisionapratama','','2021-05-31 11:32:34','https://www.instagram.com/p/CPZzXlWHtjF/','17841426077494261','Instagram','17969463505401454.jpg',NULL,NULL),(15,'17869917284433791','invisionapratama','Improvisasi','2021-06-01 01:26:01','https://www.instagram.com/p/CPkTax5HCFj/','17841426077494261','Instagram','17869917284433791.jpg',NULL,NULL),(10015,'17967463423405401','invisionapratama','Ini kapal karam','2021-06-02 11:37:50','https://www.instagram.com/p/CPPgDo8HEqn/','17841426077494261','Instagram','17967463423405401.jpg',NULL,NULL),(10016,'374215903210349_831517244146877','Invision Astrindo Pratama','Hi #Gaeesss ikuti langkah-langkah berikut ya agar kamu dapat mengajukan permohonan  kepada kami, untuk informasi lebih lanjut kunjungi:\n.\nhttps://google.com\n.\n#Indonesia\n#corona','2021-06-03 10:49:23','https://www.facebook.com/InvisionAP/photos/a.488464041785534/831517244146877/?type=3','374215903210349','Facebook','20210603104916.jpeg','ICC Admin',NULL),(10023,'1400463163070373903','InvisionAP','widget chat #chat #newfeature https://t.co/sVhwvAtoYD','2021-06-03 21:45:50','https://t.co/sVhwvAtoYD','1176382758257512448','Twitter','20210603214552447.jpg',NULL,NULL),(10024,'1399295782222655488','InvisionAP','Ini parkir https://t.co/lthE3MBvsl','2021-06-04 13:47:49','https://t.co/lthE3MBvsl','1176382758257512448','Twitter','20210604134752394.jpg',NULL,NULL),(10025,'1400712074070687745','MalikDev2020','mention get sosmed one @InvisionAP','2021-06-04 14:12:37','','1286154725725937664','Twitter','20210604143327540.mp4',NULL,NULL),(10026,'374215903210349_833846917247243','Tariman Tarikan','Hai saya tariman','2021-06-07 09:50:41','','3318350941551586','Facebook','',NULL,NULL),(10027,'374215903210349_833847293913872','Nurmansyah Candra S','ini pOst 2 ya','2021-06-07 09:52:00','','3977069165701618','Facebook','',NULL,NULL),(10028,'374215903210349_833858290579439','Nurmansyah Candra S','ini P3 ya','2021-06-07 10:19:25','','3977069165701618','Facebook','',NULL,NULL),(10029,'374215903210349_833967937235141','Nurmansyah Candra S','abcd','2021-06-07 14:28:51','','3977069165701618','Facebook','',NULL,NULL),(10030,'374215903210349_833968533901748','Sakha Wistara','Aloha kk','2021-06-07 14:30:15','','3839738052815556','Facebook','',NULL,NULL),(10031,'1402520568214159360','billaanindita2','@InvisionAP hai emot ??','2021-06-09 13:58:55','','4340922074','Twitter','',NULL,NULL),(10032,'1402540702525845504','djkelabu','@InvisionAP halo saya mention','2021-06-09 15:18:56','','1088713123534536704','Twitter','',NULL,NULL),(10033,'1400463742584778752','billaanindita2','@InvisionAP wow product baru kah?\ngood job, ini akan memudahkan customer','2021-06-09 15:20:10','','4340922074','Twitter','',NULL,NULL),(10034,'374215903210349_834877647144170','Invision Astrindo Pratama','beverly hills','2021-06-09 15:22:51','https://www.facebook.com/InvisionAP/photos/a.488464041785534/834877647144170/?type=3','374215903210349','Facebook','20210609152243.jpeg','ICC Admin',NULL),(10035,'1402541974205308931','InvisionAP','interior https://t.co/mWILCgHafw','2021-06-09 15:24:02','https://twitter.com/i/web/status/1402541974205308931','1176382758257512448','Twitter','20210609152354.jpeg','ICC Admin',NULL),(10036,'1403177437249171462','MalikDev2020','status twitt @InvisionAP','2021-06-11 09:29:05','','1286154725725937664','Twitter','',NULL,NULL),(10037,'374215903210349_826527687979166','Invision Astrindo Pratama','pesawat landing','2021-05-25 04:07:33','https://www.facebook.com/InvisionAP/posts/826527687979166','374215903210349','Facebook','374215903210349_826527687979166.mp4',NULL,NULL),(10038,'4107204022634984_123585139892614','Invision Astrindo Pratama','selemat siang Invision Astrindo Pratama','2021-06-18 04:21:22','https://www.facebook.com/107882151462913/posts/123585139892614','374215903210349','Facebook','',NULL,NULL),(10039,'4107204022634984_123610796556715','Invision Astrindo Pratama','test image mention Invision Astrindo Pratama','2021-06-18 06:35:12','https://www.facebook.com/107882151462913/posts/123610796556715','374215903210349','Facebook','',NULL,NULL),(10040,'107882151462913_123585139892614','Invision Astrindo Pratama','selemat siang Invision Astrindo Pratama','2021-06-18 04:21:22','https://www.facebook.com/107882151462913/posts/123585139892614','374215903210349','Facebook','',NULL,NULL),(10041,'107882151462913_124413259809802','Invision Astrindo Pratama','test image Invision Astrindo Pratama','2021-06-21 03:52:15','https://www.facebook.com/107882151462913/posts/124413259809802','374215903210349','Facebook','',NULL,NULL),(10042,'107882151462913_124414963142965','Invision Astrindo Pratama','new post img Invision Astrindo Pratama','2021-06-21 04:04:21','https://www.facebook.com/107882151462913/posts/124414963142965','374215903210349','Facebook','',NULL,NULL),(10043,'4107204022634984_124417226476072','Invision Astrindo Pratama','gambar new mention fb Invision Astrindo Pratama','2021-06-21 04:14:54','https://www.facebook.com/107882151462913/posts/124417226476072','374215903210349','Facebook','4107204022634984_124417226476072.jpg',NULL,NULL),(10044,'2606648856063519_1146383139209048','Invision Astrindo Pratama','siang test mention FB Invision Astrindo Pratama','2021-06-21 04:37:58','https://www.facebook.com/1106883506492345/posts/1146383139209048','374215903210349','Facebook','',NULL,NULL),(10045,'374215903210349_841422763156325','Invision Astrindo Pratama','post 1','2021-06-21 18:02:28','https://www.facebook.com/374215903210349/posts/841422763156325/','374215903210349','Facebook','','ICC Admin',NULL),(10046,'374215903210349_843022022996399','Invision Astrindo Pratama','test post 2','2021-06-24 14:19:12','https://www.facebook.com/InvisionAP/photos/a.488464041785534/843022022996399/?type=3','374215903210349','Facebook','20210624141902.jpeg','ICC Admin',NULL);

#
# Structure for table "getsosmed_three"
#

DROP TABLE IF EXISTS `getsosmed_three`;
CREATE TABLE `getsosmed_three` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `P_ID_Post` varchar(50) DEFAULT NULL,
  `C_ID_Post` varchar(50) DEFAULT NULL,
  `R_User_Name` varchar(50) DEFAULT NULL,
  `R_Message` longtext,
  `R_ID_Post` varchar(50) DEFAULT NULL,
  `R_ddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

#
# Data for table "getsosmed_three"
#

INSERT INTO `getsosmed_three` VALUES (1,'374215903210349_827098514588750','827098514588750_829981944300407','Dev Malik','Invision Astrindo Pratama yook','827098514588750_830973690867899','2021-06-02 11:33:23'),(2,'374215903210349_827098514588750','827098514588750_829981944300407','Dev Malik','Invision Astrindo Pratama hihi','827098514588750_830974117534523','2021-06-02 11:34:24'),(3,'374215903210349_827098514588750','827098514588750_829981944300407','Dev Malik','Invision Astrindo Pratama ooo090','827098514588750_830974640867804','2021-06-02 11:36:13'),(4,'374215903210349_831517244146877','831517244146877_831517487480186','Nurmansyah Candra S','Invision Astrindo Pratama ok terima kasik kk akan saya coba<br><br>thx ??','831517244146877_831527434145858','2021-06-03 11:19:14'),(5,'374215903210349_831517244146877','831517244146877_831517487480186','Nurmansyah Candra S','ini cara apa ya','831517244146877_833749820590286','2021-06-07 12:16:29'),(6,'374215903210349_831517244146877','831517244146877_831517487480186','Nurmansyah Candra S','','831517244146877_833749820590286','2021-06-07 12:20:06'),(7,'374215903210349_831517244146877','831517244146877_833749460590322','Nurmansyah Candra S','ini cek loc','831517244146877_833757707256164','2021-06-07 12:34:50'),(8,'374215903210349_831517244146877','831517244146877_833749460590322','Nurmansyah Candra S','c2 cek PL','831517244146877_833805977251337','2021-06-07 14:52:19'),(9,'374215903210349_827098514588750','827098514588750_833832747248660','Nurmansyah Candra S','ini rep rumah c3','827098514588750_833833017248633','2021-06-07 16:09:22'),(10,'374215903210349_831517244146877','831517244146877_833832697248665','Nurmansyah Candra S','ini rep c3','831517244146877_833833077248627','2021-06-07 16:09:34'),(11,'374215903210349_833846917247243','','Tariman Tarikan','Hai saya tariman','','2021-06-07 16:50:41'),(12,'374215903210349_833846917247243','','Tariman Tarikan','Hai saya tariman','','2021-06-07 16:50:41'),(13,'374215903210349_833847293913872','','Nurmansyah Candra S','ini pOst 2 ya','','2021-06-07 16:52:00'),(14,'374215903210349_833847293913872','','Nurmansyah Candra S','ini pOst 2 ya','','2021-06-07 16:52:00'),(15,'374215903210349_833858290579439','833858290579439_833859523912649','Nurmansyah Candra S','Tariman Tarikan reply P3','833858290579439_833860430579225','2021-06-07 17:23:54'),(16,'374215903210349_833858290579439','833858290579439_833859523912649','Nurmansyah Candra S','reply p3 ini ya komen p3','833858290579439_833861843912417','2021-06-07 17:26:57'),(17,'374215903210349_833858290579439','833858290579439_833861317245803','Nurmansyah Candra S','Tariman Tarikan reply komen lagi p3 ','833858290579439_833862097245725','2021-06-07 17:27:32'),(18,'374215903210349_831517244146877','831517244146877_831517487480186','Sakha Wistara','Nurmansyah Candra S Reply C1 Ah','831517244146877_833966403901961','2021-06-07 21:25:55'),(19,'374215903210349_833858290579439','833858290579439_833861317245803','Nurmansyah Candra S','y','833858290579439_833966717235263','2021-06-07 21:26:32'),(20,'374215903210349_833847293913872','833847293913872_833850467246888','Sakha Wistara','Nurmansyah Candra S haha','833847293913872_833967813901820','2021-06-07 21:28:37'),(21,'374215903210349_833847293913872','833847293913872_833850467246888','Sakha Wistara','haha haha reply reply','833847293913872_833967887235146','2021-06-07 21:28:45');

#
# Structure for table "getsosmed_two"
#

DROP TABLE IF EXISTS `getsosmed_two`;
CREATE TABLE `getsosmed_two` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `P_ID_Post` varchar(50) DEFAULT NULL,
  `C_User_Name` varchar(50) DEFAULT NULL,
  `C_Message` longtext,
  `C_ID_Post` varchar(50) DEFAULT NULL,
  `C_ddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

#
# Data for table "getsosmed_two"
#

INSERT INTO `getsosmed_two` VALUES (1,'374215903210349_827098514588750','Dev Malik','icon test','827098514588750_828780004420601','2021-05-29 11:43:02'),(2,'374215903210349_827098514588750','Dev Malik','test video 123','827098514588750_828780631087205','2021-05-29 11:45:05'),(3,'374215903210349_827098514588750','Dev Malik','123 Invision Astrindo Pratama','827098514588750_829981944300407','2021-05-31 16:45:31'),(4,'374215903210349_831517244146877','Nurmansyah Candra S','caranya bagaimana kk C1','831517244146877_831517487480186','2021-06-03 10:50:05'),(5,'374215903210349_831517244146877','Nurmansyah Candra S','coba C2','831517244146877_831584364140165','2021-06-03 14:05:16'),(6,'374215903210349_831517244146877','Nurmansyah Candra S','C 1 <br><br>kirim PL ??','831517244146877_833749460590322','2021-06-07 12:15:21'),(7,'374215903210349_831517244146877','Nurmansyah Candra S','Invision Astrindo Pratama hai hai','831517244146877_833807747251160','2021-06-07 14:55:51'),(8,'374215903210349_827098514588750','Nurmansyah Candra S','rumah','827098514588750_833816573916944','2021-06-07 15:19:08'),(9,'374215903210349_831517244146877','Nurmansyah Candra S','ini c3','831517244146877_833832697248665','2021-06-07 16:08:30'),(10,'374215903210349_827098514588750','Nurmansyah Candra S','ini rumah c3','827098514588750_833832747248660','2021-06-07 16:08:38'),(11,'374215903210349_833847293913872','Nurmansyah Candra S','C2 post 2','833847293913872_833850467246888','2021-06-07 17:01:07'),(12,'374215903210349_833858290579439','Tariman Tarikan','Komen p3','833858290579439_833859523912649','2021-06-07 17:22:04'),(13,'374215903210349_833858290579439','Tariman Tarikan','Komen lagi P3','833858290579439_833861317245803','2021-06-07 17:25:53'),(14,'374215903210349_833858290579439','Nurmansyah Candra S','comen','833858290579439_833864267245508','2021-06-07 17:32:00'),(15,'374215903210349_831517244146877','Nurmansyah Candra S','haai ini comment 1','831517244146877_834332210532047','2021-06-08 14:23:42'),(16,'374215903210349_831517244146877','Misykah Nova Hamidah','halo selamat siang feed','831517244146877_834875097144425','2021-06-09 15:15:29'),(17,'374215903210349_826527687979166','Dev Malik','hai','826527687979166_839668783331723','2021-06-18 10:51:10');

#
# Structure for table "mcategory"
#

DROP TABLE IF EXISTS `mcategory`;
CREATE TABLE `mcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryID` varchar(10) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Description` longtext,
  `NA` varchar(10) DEFAULT NULL,
  `UserCreate` varchar(250) DEFAULT NULL,
  `DateCreate` datetime DEFAULT NULL,
  `UserUpdate` varchar(250) DEFAULT NULL,
  `DateUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

#
# Data for table "mcategory"
#

INSERT INTO `mcategory` VALUES (31,'CAT-10002','Complaint',NULL,'Y','1002201401','2014-03-25 11:07:36','AGENT1','2019-06-19 16:34:33'),(32,'CAT-10003','Inquiry',NULL,'N','AGENT1','2019-06-19 09:34:15','endyrayn','2019-10-09 11:33:43'),(33,'CAT-10004','Request',NULL,'Y','AGENT1','2019-06-19 14:20:44','endyrayn','2019-10-09 11:34:43'),(34,'CAT-10005','Informasi',NULL,'Y','AGENT1','2019-07-30 15:40:58','agent1','2019-09-27 16:33:32'),(35,'CAT-10006','OTHER',NULL,'N','AGENT1','2019-07-31 18:28:22','endyrayn','2019-10-09 11:33:55'),(36,'CAT-10007','Saran',NULL,'Y','endyrayn','2019-10-09 11:32:43',NULL,NULL),(37,'CAT-10008','Misscellineous',NULL,'Y','endyrayn','2019-10-09 11:33:22',NULL,NULL);

#
# Structure for table "mcustomer"
#

DROP TABLE IF EXISTS `mcustomer`;
CREATE TABLE `mcustomer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Tittle` varchar(250) DEFAULT NULL,
  `CustomerID` varchar(100) NOT NULL,
  `NOKTP` varchar(250) DEFAULT NULL,
  `Name` text,
  `Birth` date DEFAULT NULL,
  `JenisKelamin` varchar(50) DEFAULT NULL,
  `NamaPerusahaan` varchar(150) DEFAULT NULL,
  `Telepon` varchar(20) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Alamat` longtext,
  `City` varchar(50) DEFAULT NULL,
  `Region` varchar(50) DEFAULT NULL,
  `NetworkSocial` varchar(50) DEFAULT NULL,
  `CusStatus` varchar(50) DEFAULT NULL,
  `Facebook` varchar(100) DEFAULT NULL,
  `Twitter` varchar(100) DEFAULT NULL,
  `Kaskus` varchar(100) DEFAULT NULL,
  `Home` varchar(50) DEFAULT NULL,
  `Office` varchar(50) DEFAULT NULL,
  `HP` varchar(50) DEFAULT NULL,
  `Others` longtext,
  `Pesan` longtext,
  `AlamatIP` varchar(200) DEFAULT NULL,
  `SiteID` varchar(150) DEFAULT NULL,
  `SitePassword` varchar(50) DEFAULT NULL,
  `SiteIP` varchar(50) DEFAULT NULL,
  `Login` int(11) DEFAULT NULL,
  `path` text,
  `UserCreateCustomer` text,
  `DateCreateCustomer` datetime DEFAULT NULL,
  `SourceCreate` varchar(50) DEFAULT NULL,
  `UserUpdate` text,
  `DateUpdate` datetime DEFAULT NULL,
  `StatusUpdated` varchar(50) DEFAULT NULL,
  `DateLastUpdated` datetime DEFAULT NULL,
  `Status` varchar(50) NOT NULL,
  `Relations` varchar(150) DEFAULT NULL,
  `CompID` varchar(50) DEFAULT NULL,
  `Nomor_Rekening` varchar(250) DEFAULT NULL,
  `Cabang` text,
  `Instagram` varchar(100) DEFAULT NULL,
  `flag` varchar(50) DEFAULT NULL,
  `SyncData` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`,`CustomerID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19263 DEFAULT CHARSET=latin1;

#
# Data for table "mcustomer"
#

INSERT INTO `mcustomer` VALUES (18213,NULL,'20210519104009',NULL,'Malik Riski',NULL,NULL,NULL,NULL,'Maliki@gmail.com',NULL,'pejaten barat',NULL,NULL,'0','Non Nasabah',NULL,NULL,NULL,NULL,NULL,'08136766443',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-19 10:40:10','Manual','Agent1','2021-05-19 13:46:16','No',NULL,'REGISTER',NULL,'0',NULL,'00017',NULL,NULL,'Y'),(18217,NULL,'20210519131242',NULL,'Misykah Nova Hamidah',NULL,NULL,NULL,NULL,'misykah.nova.hamidah@gmail.com',NULL,'Pondok Gede',NULL,NULL,'0','',NULL,NULL,NULL,NULL,NULL,'0817273553',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-19 13:12:42','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'',NULL,NULL,'Y'),(18219,NULL,'20210519132159',NULL,'Jajang Deny Ardiansyah',NULL,NULL,NULL,NULL,'djkelabu@gmail.com',NULL,'Pondok Gede',NULL,NULL,'0','Syariah',NULL,NULL,NULL,NULL,NULL,'0811813553',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-19 13:21:59','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'00001',NULL,NULL,'Y'),(18223,NULL,'20210519133138',NULL,'Yurika Oktavianti',NULL,NULL,NULL,NULL,'Yurika@gmail.com',NULL,'Pondok Gede',NULL,NULL,'0','Prioritas',NULL,NULL,NULL,NULL,NULL,'08112018765',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-19 13:31:39','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'00001',NULL,NULL,'Y'),(18225,NULL,'20210519135319',NULL,'Red Bionic',NULL,NULL,NULL,NULL,'RedBionic@Gmail.com',NULL,'blok m',NULL,NULL,'0','Non Nasabah',NULL,NULL,NULL,NULL,NULL,'086709790234',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-19 13:53:19','Manual','Agent1','2021-05-20 13:41:16','No',NULL,'REGISTER',NULL,'0',NULL,'00002',NULL,NULL,'Y'),(18227,NULL,'20210520090157',NULL,'Samsu Rizal',NULL,NULL,NULL,NULL,'rizalsamsurizal708@gmail.com',NULL,'Bogor',NULL,NULL,'0','Nasabah',NULL,NULL,NULL,NULL,NULL,'085782431208',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-20 09:01:58','Manual','Agent1','2021-05-20 11:48:03','No',NULL,'REGISTER',NULL,'0',NULL,'00015',NULL,NULL,'Y'),(18231,NULL,'20210520110723',NULL,'Nurmansyah',NULL,NULL,NULL,NULL,'nurmansyah@gmail.com',NULL,'Sunter',NULL,NULL,'0','Non Nasabah',NULL,NULL,NULL,NULL,NULL,'081123456753',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-20 11:07:24','Manual','Agent1','2021-05-20 13:34:11','No',NULL,'REGISTER',NULL,'0',NULL,'00001',NULL,NULL,'Y'),(18235,NULL,'3453488991401068',NULL,'Siti Robitoh',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,'3453488991401068',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-20 12:54:29',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(18236,NULL,'20210520134035',NULL,'Restu Ramadhika',NULL,NULL,NULL,NULL,'Restu.Ramadhika@gmail.com',NULL,'Parung',NULL,NULL,'0','Non Nasabah',NULL,NULL,NULL,NULL,NULL,'081123234344',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-20 13:40:35','Manual','Agent1','2021-05-20 14:04:50','No',NULL,'REGISTER',NULL,'0',NULL,'00001',NULL,NULL,'Y'),(18238,NULL,'374215903210349',NULL,'Invision Astrindo Pratama',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,'374215903210349',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-20 16:24:03',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(18239,NULL,'20210520162648',NULL,'Nurman',NULL,NULL,NULL,NULL,'',NULL,'',NULL,NULL,'0','',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-20 16:26:48','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'',NULL,NULL,'Y'),(18240,NULL,'',NULL,'',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-20 17:12:31',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(18242,NULL,'2643067769084636',NULL,'Badu Dermawan',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,'2643067769084636',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-20 19:30:01',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(18246,NULL,'3318350941551586',NULL,'Tariman Tarikan',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,'3318350941551586',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-21 14:20:11',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(18247,NULL,'20210522073912',NULL,'Samsu Rizal',NULL,NULL,NULL,NULL,'',NULL,'',NULL,NULL,'0','',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-22 07:39:12','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'',NULL,NULL,'Y'),(18248,NULL,'20210522145623',NULL,'Dev Malik',NULL,NULL,NULL,NULL,'',NULL,'',NULL,NULL,'0','',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-22 14:56:24','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'',NULL,NULL,'Y'),(19225,NULL,'20210524134943719',NULL,'endy_badry',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-24 13:49:44',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,'endy_badry',NULL,'N'),(19233,NULL,'20210526094505',NULL,'Billaa Nindita',NULL,NULL,NULL,NULL,'',NULL,'Sunter',NULL,NULL,'0','Nasabah',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-26 09:45:05','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'00001',NULL,NULL,'Y'),(19234,NULL,'4107452729314637',NULL,'Yurika Oktavianti',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,'4107452729314637',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-26 10:31:49',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(19240,NULL,'4068255369886921',NULL,'Rudi Suprayogi',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,'4068255369886921',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-27 11:05:32',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(19242,NULL,'20210527111248',NULL,'Yasa Wijaya',NULL,NULL,NULL,NULL,'Abu.Yazeed@gmail.com',NULL,'',NULL,NULL,'0','',NULL,NULL,NULL,NULL,NULL,'0811547585996',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-05-27 11:12:48','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'',NULL,NULL,'Y'),(19254,NULL,'401245832',NULL,'resturamadhika',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,NULL,'401245832',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-05-31 21:06:47',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(19255,NULL,'20210602113928',NULL,'Salsa Billa',NULL,NULL,NULL,NULL,'',NULL,'',NULL,NULL,'0','Non Nasabah',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'Agent1','2021-06-02 11:39:30','Manual',NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,'00001',NULL,NULL,'Y'),(19257,NULL,'3839738052815556',NULL,'Sakha Wistara',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,'3839738052815556',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-06-07 21:26:05',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N'),(19262,NULL,'107882151462913_123585139892614',NULL,' ',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'0',NULL,'107882151462913_123585139892614',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'system sosmed','2021-06-18 12:48:24',NULL,NULL,NULL,'No',NULL,'REGISTER',NULL,'0',NULL,NULL,NULL,NULL,'N');

#
# Structure for table "mcustomerchannel"
#

DROP TABLE IF EXISTS `mcustomerchannel`;
CREATE TABLE `mcustomerchannel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` varchar(50) DEFAULT NULL,
  `FlagChannel` varchar(30) DEFAULT NULL,
  `ValueChannel` varchar(200) DEFAULT NULL,
  `DateCreate` datetime DEFAULT NULL,
  `UserCreate` varchar(200) DEFAULT NULL,
  `DateUpdate` datetime DEFAULT NULL,
  `UserUpdate` varchar(200) DEFAULT NULL,
  `Status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6148 DEFAULT CHARSET=latin1;

#
# Data for table "mcustomerchannel"
#

INSERT INTO `mcustomerchannel` VALUES (5132,'20210519101234','Instagram','misykah.nova.hamidah','2021-05-19 10:12:35','Agent1',NULL,NULL,'Y'),(5134,'20210519131242','Instagram','misykah.nova.hamidah','2021-05-19 13:12:42','Agent1',NULL,NULL,'Y'),(5135,'20210519131242','Facebook','3095994283789261','2021-05-19 13:19:00','Agent1',NULL,NULL,'Y'),(5136,'20210519132159','Instagram','djkelabu','2021-05-19 13:21:59','Agent1',NULL,NULL,'Y'),(5137,'20210519132159','Twitter','270847654','2021-05-19 13:24:53','Agent1',NULL,NULL,'Y'),(5138,'20210519131242','Facebook','4003363643104835','2021-05-19 13:27:25','Agent1',NULL,NULL,'Y'),(5139,'20210519132159','Facebook','2723449297687843','2021-05-19 13:29:48','Agent1',NULL,NULL,'Y'),(5140,'20210519133138','Instagram','riuke.official','2021-05-19 13:31:39','Agent1',NULL,NULL,'Y'),(5141,'20210519132159','Facebook','4295546270519795','2021-05-19 13:35:53','Agent1',NULL,NULL,'Y'),(5142,'20210519104009','Instagram','malikdev2020','2021-05-19 13:39:39','Agent1','2021-05-19 13:46:16','Agent1','Y'),(5143,'20210519135319','Facebook','2606648856063519','2021-05-19 13:53:19','Agent1','2021-05-20 13:41:16','Agent1','Y'),(5144,'20210520090157','Instagram','samsu6103','2021-05-20 09:01:58','Agent1','2021-05-20 09:02:20','Agent1','Y'),(5145,'20210520110723','Instagram','nurmansyahchunkz','2021-05-20 11:07:24','Agent1','2021-05-20 12:04:24','Agent1','Y'),(5146,'20210520134035','Instagram','resturamadhi_ka','2021-05-20 11:47:11','Agent1','2021-05-20 14:04:50','Agent1','Y'),(5147,'20210519132159','Facebook','3049392258491630','2021-05-20 11:56:11','Agent1',NULL,NULL,'Y'),(5148,'20210519132159','Facebook','2991466384221786','2021-05-20 11:56:24','Agent1',NULL,NULL,'Y'),(5149,'20210519132159','Facebook','4279951648693358','2021-05-20 12:06:33','Agent1',NULL,NULL,'Y'),(5150,'20210519104009','Twitter','1286154725725937664','2021-05-20 13:41:43','Agent1',NULL,NULL,'Y'),(5151,'20210520162648','Facebook','3977069165701618','2021-05-20 16:26:48','Agent1',NULL,NULL,'Y'),(5152,'20210519133138','Facebook','2254823114629433','2021-05-20 17:53:47','Agent1',NULL,NULL,'Y'),(5153,'20210522073912','Facebook','3274283266007975','2021-05-22 07:39:12','Agent1',NULL,NULL,'Y'),(5154,'20210522145623','Facebook','4107204022634984','2021-05-22 14:56:24','Agent1',NULL,NULL,'Y'),(6143,'20210526094505','Twitter','4340922074','2021-05-26 09:45:05','Agent1',NULL,NULL,'Y'),(6144,'20210527111248','Facebook','4177492318983446','2021-05-27 11:12:48','Agent1',NULL,NULL,'Y'),(6145,'20210602113928','Facebook','4059832654053481','2021-06-02 11:39:30','Agent1',NULL,NULL,'Y'),(6146,'20210519131242','Facebook','4689018114450327','2021-06-28 12:18:22','Agent1',NULL,NULL,'Y'),(6147,'20210519132159','Twitter','1088713123534536704','2021-06-28 12:18:39','Agent1',NULL,NULL,'Y');

#
# Structure for table "menu_access"
#

DROP TABLE IF EXISTS `menu_access`;
CREATE TABLE `menu_access` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(50) DEFAULT NULL,
  `MenuID` int(11) DEFAULT NULL,
  `SubMenuID` varchar(50) DEFAULT NULL,
  `MenuIDTree` varchar(50) DEFAULT NULL,
  `LevelUserID` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=385123 DEFAULT CHARSET=latin1;

#
# Data for table "menu_access"
#

INSERT INTO `menu_access` VALUES (385058,'agent1',1011,'4816',NULL,NULL,NULL,NULL),(385059,'agent1',1011,'4817',NULL,NULL,NULL,NULL),(385060,'agent1',1011,'4818',NULL,NULL,NULL,NULL),(385061,'agent1',1011,'4858',NULL,NULL,NULL,NULL),(385062,'agent1',1011,NULL,NULL,NULL,NULL,NULL),(385063,'agent1',1015,'4833',NULL,NULL,NULL,NULL),(385064,'agent1',1015,'4834',NULL,NULL,NULL,NULL),(385065,'agent1',1015,NULL,NULL,NULL,NULL,NULL),(385066,'agent1',1008,'4801',NULL,NULL,NULL,NULL),(385067,'agent1',1008,'4802',NULL,NULL,NULL,NULL),(385068,'agent1',1008,'4803',NULL,NULL,NULL,NULL),(385069,'agent1',1008,'4804',NULL,NULL,NULL,NULL),(385070,'agent1',1008,'4805',NULL,NULL,NULL,NULL),(385071,'agent1',1008,'4808',NULL,NULL,NULL,NULL),(385072,'agent1',1008,'4812',NULL,NULL,NULL,NULL),(385073,'agent1',1008,'4836',NULL,NULL,NULL,NULL),(385074,'agent1',1008,'4839',NULL,NULL,NULL,NULL),(385075,'agent1',1008,'4840',NULL,NULL,NULL,NULL),(385076,'agent1',1008,'4841',NULL,NULL,NULL,NULL),(385077,'agent1',1008,'4847',NULL,NULL,NULL,NULL),(385078,'agent1',1008,'4848',NULL,NULL,NULL,NULL),(385079,'agent1',1008,'4849',NULL,NULL,NULL,NULL),(385080,'agent1',1008,'4850',NULL,NULL,NULL,NULL),(385081,'agent1',1008,'4851',NULL,NULL,NULL,NULL),(385082,'agent1',1008,'4852',NULL,NULL,NULL,NULL),(385083,'agent1',1008,'4853',NULL,NULL,NULL,NULL),(385084,'agent1',1008,'4854',NULL,NULL,NULL,NULL),(385085,'agent1',1008,'4855',NULL,NULL,NULL,NULL),(385086,'agent1',1008,'4856',NULL,NULL,NULL,NULL),(385087,'agent1',1008,NULL,NULL,NULL,NULL,NULL),(385088,'agent1',1012,NULL,NULL,NULL,NULL,NULL),(385089,'agent1',1010,'4819','292964',NULL,NULL,NULL),(385090,'agent1',1010,'4819','292965',NULL,NULL,NULL),(385091,'agent1',1010,'4819','292983',NULL,NULL,NULL),(385092,'agent1',1010,'4819','292989',NULL,NULL,NULL),(385093,'agent1',1010,'4819',NULL,NULL,NULL,NULL),(385094,'agent1',1010,'4843','292984',NULL,NULL,NULL),(385095,'agent1',1010,'4843','292985',NULL,NULL,NULL),(385096,'agent1',1010,'4843',NULL,NULL,NULL,NULL),(385097,'agent1',1010,'4844',NULL,NULL,NULL,NULL),(385098,'agent1',1010,'4845',NULL,NULL,NULL,NULL),(385099,'agent1',1010,'4846','292981',NULL,NULL,NULL),(385100,'agent1',1010,'4846','292982',NULL,NULL,NULL),(385101,'agent1',1010,'4846','293990',NULL,NULL,NULL),(385102,'agent1',1010,'4846','294990',NULL,NULL,NULL),(385103,'agent1',1010,'4846','294992',NULL,NULL,NULL),(385104,'agent1',1010,'4846',NULL,NULL,NULL,NULL),(385105,'agent1',1010,NULL,NULL,NULL,NULL,NULL),(385106,'agent1',1009,'4814',NULL,NULL,NULL,NULL),(385107,'agent1',1009,'4815',NULL,NULL,NULL,NULL),(385108,'agent1',1009,'4842',NULL,NULL,NULL,NULL),(385109,'agent1',1009,NULL,NULL,NULL,NULL,NULL),(385110,'agent1',1013,'4827',NULL,NULL,NULL,NULL),(385111,'agent1',1013,'4859',NULL,NULL,NULL,NULL),(385112,'agent1',1013,NULL,NULL,NULL,NULL,NULL),(385113,'agent1',1014,'4826',NULL,NULL,NULL,NULL),(385114,'agent1',1014,'4860',NULL,NULL,NULL,NULL),(385115,'agent1',1014,NULL,NULL,NULL,NULL,NULL),(385116,'agent1',1018,NULL,NULL,NULL,NULL,NULL),(385117,'agent1',1019,NULL,NULL,NULL,NULL,NULL),(385118,'agent1',1016,NULL,NULL,NULL,NULL,NULL),(385121,'agent100',1014,NULL,NULL,NULL,NULL,NULL),(385122,'agent100',1014,'4826',NULL,NULL,NULL,NULL);

#
# Structure for table "menu1"
#

DROP TABLE IF EXISTS `menu1`;
CREATE TABLE `menu1` (
  `MenuID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(100) DEFAULT NULL,
  `Url` varchar(250) DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `Icon` varchar(100) DEFAULT NULL,
  `DivID` varchar(10) DEFAULT NULL,
  `Activity` varchar(5) DEFAULT NULL,
  `MenuSection` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`MenuID`)
) ENGINE=InnoDB AUTO_INCREMENT=1020 DEFAULT CHARSET=latin1;

#
# Data for table "menu1"
#

INSERT INTO `menu1` VALUES (1008,'Master Data','master-data',3,'master-data','MD','N','Data'),(1009,'Ticket','ticket',6,'ticket','TC','N','Feature'),(1010,'Channel','channel',5,'channel','CH','N','Feature'),(1011,'Dashboard','dashboard',1,'dashboard','DB','N','Dashboard'),(1012,'Todolist','todolist',4,'todolist','TD','N','Feature'),(1013,'Report','report',8,'report','RP','N','Feature'),(1014,'Management User','management-user',9,'management-user','UM','N','Others'),(1015,'Customer','customer',2,'customer','CT','N','Data'),(1016,'Knowledge Base','knowledge-base',11,'knowledge-base','KB','N','Others'),(1018,'Setting','setting',10,'setting',NULL,'N','Others'),(1019,'Web Url Call','web-call-url',7,'web-call-url',NULL,'N','Feature');

#
# Structure for table "menu2"
#

DROP TABLE IF EXISTS `menu2`;
CREATE TABLE `menu2` (
  `SubMenuID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuID` int(11) DEFAULT NULL,
  `SubMenuName` varchar(250) DEFAULT NULL,
  `Url` varchar(250) DEFAULT NULL,
  `DivID` varchar(250) DEFAULT NULL,
  `Icon` varchar(150) DEFAULT NULL,
  `Param` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`SubMenuID`)
) ENGINE=InnoDB AUTO_INCREMENT=4861 DEFAULT CHARSET=latin1;

#
# Data for table "menu2"
#

INSERT INTO `menu2` VALUES (4801,1008,'Jenis Kategori','Transaction_Type.aspx?page=mst_tt&idpage=1008','TT',NULL,'N'),(4802,1008,'Group Kategori Produk','calltype_one.aspx?page=mst_ctone&idpage=1008','BR',NULL,'N'),(4803,1008,'Kategori Produk','calltype_two.aspx?page=mst_cttwo&idpage=1008','PR',NULL,'N'),(4804,1008,'Kategori Permasalahan','calltype_tre.aspx?page=mst_cttre&idpage=1008\t','PL',NULL,'N'),(4805,1008,'Status','status.aspx?page=mst_ss&idpage=1008','ST',NULL,'N'),(4808,1008,'Data Channel','sourcetype.aspx?page=sct_typ&idpage=1008','SE',NULL,'N'),(4812,1008,'Unit Kerja Permasalahan','mDept.aspx?page=dept&idpage=1008','DP',NULL,'N'),(4814,1009,'Create Complaint','tr_utama.aspx?idpage=1009','CT',NULL,'N'),(4815,1009,'History Complaint','history_ticket.aspx?page=tk_history&idpage=1009','TH',NULL,'N'),(4816,1011,'Agent','../dashboard/dashboard3.html','DA',NULL,'Y'),(4817,1011,'Supervisor','../dashboard/dashboard2.html','DS',NULL,'Y'),(4818,1011,'Administrator','../dashboard/dashboard1.html','DM',NULL,'Y'),(4819,1010,'Email','','IE',NULL,'N'),(4826,1014,'User Setting Privilege','privilege','UP',NULL,'N'),(4827,1013,'Report Default','rptcoba.aspx?page=rpt_coba&idpage=1013','',NULL,'N'),(4833,1015,'Add Customer','addcrnumber.aspx?idpage=1015',NULL,NULL,'N'),(4834,1015,'Preview Customer','customer.aspx?&idpage=1015',NULL,NULL,'N'),(4836,1008,'Data Login Activity','login_activity.aspx?idpage=1008',NULL,NULL,'N'),(4839,1008,'Data BI','BTN_mBI.aspx?page=btn_master&idpage=1008','BTN',NULL,'N'),(4840,1008,'Data OJK','BTN_mOJK.aspx?page=btn_master&idpage=1008','BTN',NULL,'N'),(4841,1008,'Mapping Report BI','BTN_trxReport.aspx?page=btn_master&idpage=1008','BTN',NULL,'N'),(4842,1009,'Add SLA','add_sla.aspx?idpage=1009','AS',NULL,'N'),(4843,1010,'Outbound','',NULL,NULL,'N'),(4844,1010,'Sms Blast','importsms.aspx?idpage=1008',NULL,NULL,'N'),(4845,1010,'Email Blast','importemail.aspx?idpage=1008',NULL,NULL,'N'),(4846,1010,'Sosmed','',NULL,NULL,'Y'),(4847,1008,'Unit Kerja Agent','unitkerja_agent.aspx?idpage=1008',NULL,NULL,'N'),(4848,1008,'Data Skala Prioritas','data_skala_prioritas.aspx?idpage=1008',NULL,NULL,'N'),(4849,1008,'Data Jenis Nasabah','datajenisnasabah.aspx?idpage=1008',NULL,NULL,'N'),(4850,1008,'Data Campaign','datacampaign.aspx?idpage=1008',NULL,NULL,'N'),(4851,1008,'Data Status Pelapor','data_status_pelapor.aspx?idpage=1008',NULL,NULL,'N'),(4852,1008,'Data Customer Type','data_customer_type.aspx?idpage=1008',NULL,NULL,'N'),(4853,1008,'Data Penyebab','data_sebab.aspx?idpage=1008',NULL,NULL,'N'),(4854,1008,'Data Aux Reason','data_aux_reason.aspx?idpage=1008',NULL,NULL,'N'),(4855,1008,'Data Indikator Warna SLA','data_indikator_warna.aspx?idpage=1008',NULL,NULL,'N'),(4856,1008,'Data Sumber Informasi','data_sumber_informasi.aspx?idpage=1008',NULL,NULL,'N'),(4858,1011,'Channel','dashboard_sosmed.aspx',NULL,NULL,'N'),(4859,1013,'Report Sosial Media','R_Sosmed.aspx?page=R_Sosmed&idpage=1013',NULL,NULL,'N'),(4860,1014,'Account Subscribe','subscribe_account.aspx?page=um_usrpre&idpage=1014',NULL,NULL,'N');

#
# Structure for table "menu3"
#

DROP TABLE IF EXISTS `menu3`;
CREATE TABLE `menu3` (
  `SubMenuIDTree` int(11) NOT NULL AUTO_INCREMENT,
  `MenuID` varchar(150) DEFAULT NULL,
  `SubMenuID` varchar(150) DEFAULT NULL,
  `MenuTreeName` varchar(50) DEFAULT NULL,
  `Url` varchar(250) DEFAULT NULL,
  `DivID` varchar(50) DEFAULT NULL,
  `Param` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`SubMenuIDTree`)
) ENGINE=InnoDB AUTO_INCREMENT=294993 DEFAULT CHARSET=latin1;

#
# Data for table "menu3"
#

INSERT INTO `menu3` VALUES (292964,'1010','4819','Inbox','inboxemailsml.aspx?status=inbox&idpage=1010','EMA_INB','N'),(292965,'1010','4819','Send','sendemail.aspx?status=send&idpage=1010','EMA_SEN','N'),(292966,'1010','4820','Data History','alltw.aspx?page=twthstr','TWT_DTH','N'),(292967,'1010','4820','Input Sentimen','inputsentimenMP.aspx?sentimen','TWT_INP_SEN','N'),(292968,'1010','4820','Keyword Setting','twitter_keyword.aspx?page=twt_ks','TWT_KYS','N'),(292969,'1010','4820','Posting Status','posting_twt.aspx?page=post_twt','TWT_PST','N'),(292970,'1010','4820','Setting Sentimen','settingsentimen.aspx?sentimenttwt','TWT_SET_SEN','N'),(292971,'1010','4821','Data History','allfb.aspx?page=fbhstr','FCB_DTH','N'),(292972,'1010','4821','Input Sentimen','inputsentimenMP.aspx?sentimen','FCB_INP_SEN','N'),(292973,'1010','4821','Keyword Setting','facebook_keyword.aspx?page=fcb_ks','FCB_KYS','N'),(292974,'1010','4821','Monitoring Posting','pantaulike.aspx?page=fcb_pl','FCB_LOP','N'),(292975,'1010','4821','Posting Status','posting_fcb.aspx?page=post_fcb','FCB_PST','N'),(292976,'1010','4821','Setting Sentimen','settingsentimen.aspx?sentimentfcb','FCB_SET_SEN','N'),(292977,'1010','4822','Inbox','fax.aspx?status=open','FAX_INB','N'),(292978,'1010','4822','Send','fax.aspx?status=send','FAX_SEN','N'),(292979,'1010','4824','Inbox','','SMS_INB','N'),(292980,'1010','4824','Send','','SMS_SEN','N'),(292981,'1010','4846','Data Inbox Sosmed','livechat_coba.aspx?idpage=1010',NULL,'N'),(292982,'1010','4846','Data Finish Sosmed','conversationchat.aspx?idpage=1010',NULL,'N'),(292983,'1010','4819','History Assign Email','assign_history_email.aspx?idpage=1010',NULL,'N'),(292984,'1010','4843','Call','outboundcall.aspx?idpage=1010',NULL,'N'),(292985,'1010','4843','History Assign Call','assign_history_outbound.aspx?idpage=1010',NULL,'N'),(292986,'1010','4857','Facebook','https://web.facebook.com',NULL,'Y'),(292987,'1010','4857','Instagram','https://www.instagram.com',NULL,'Y'),(292988,'1010','4857','Twitter','https://twitter.com',NULL,'Y'),(292989,'1010','4819','Monitoring Email','inboxemailall.aspx?idpage=1010',NULL,'N'),(293990,'1010','4846','Data History Sosmed','Temp_ChatHistory.aspx?idpage=1010',NULL,'N'),(294990,'1010','4846','Data Sosmed Post','sosmed_wallpost.aspx?idpage=1010',NULL,'N'),(294992,'1010','4846','Data List Sosmed Post','TrxPost.aspx?idpage=1010',NULL,'N');

#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2021_07_31_160334_create_products_table',2);

#
# Structure for table "mleveluser"
#

DROP TABLE IF EXISTS `mleveluser`;
CREATE TABLE `mleveluser` (
  `LevelUserID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) DEFAULT NULL,
  `Description` text,
  `NA` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`LevelUserID`)
) ENGINE=InnoDB AUTO_INCREMENT=322 DEFAULT CHARSET=latin1;

#
# Data for table "mleveluser"
#

INSERT INTO `mleveluser` VALUES (317,'layer1','Agent','Y'),(318,'layer2','CaseUnit','Y'),(319,'layer3','PICUnit','Y'),(320,'Admin','Administrator','Y'),(321,'Supervisor','Supervisor','Y');

#
# Structure for table "morganization"
#

DROP TABLE IF EXISTS `morganization`;
CREATE TABLE `morganization` (
  `ORGANIZATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUPID` varchar(50) DEFAULT NULL,
  `COMPID` varchar(50) DEFAULT NULL,
  `CHANNEL_CODE` varchar(150) DEFAULT NULL,
  `ORGANIZATION_NAME` text,
  `EMAIL` text,
  `DESCRIPTION` longtext,
  `USERCREATE` varchar(250) DEFAULT NULL,
  `DATECREATE` datetime DEFAULT NULL,
  `USERUPDATE` text,
  `DATEUPDATE` datetime DEFAULT NULL,
  `NA` varchar(5) DEFAULT NULL,
  `FLAG` int(11) DEFAULT NULL,
  PRIMARY KEY (`ORGANIZATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1054 DEFAULT CHARSET=latin1;

#
# Data for table "morganization"
#

INSERT INTO `morganization` VALUES (2,'2','001','RF','RFSD',NULL,'',NULL,NULL,'AGENT1','2019-02-11 20:11:41','Y',0),(3,'2','002','TR','TRSD',NULL,NULL,NULL,NULL,'AGENT1','2019-02-11 20:14:02','Y',0),(4,'2','003','SM','SMLD','rizkimubarok22@gmail.com',NULL,NULL,NULL,'AGENT1','2019-07-29 17:58:57','Y',0),(35,NULL,NULL,'NS','NSLD',NULL,NULL,'AGENT1',NULL,'AGENT1','2019-02-11 20:14:13','Y',0),(36,NULL,NULL,'CC','CCRD',NULL,NULL,'AGENT1',NULL,'AGENT1','2019-02-11 20:14:17','Y',0),(37,NULL,NULL,'OF','OBSD FRAUD ',NULL,NULL,'AGENT1',NULL,'AGENT1','2019-02-11 20:15:15','Y',0),(38,NULL,NULL,'WM','WMD',NULL,NULL,'AGENT1',NULL,'AGENT1','2019-02-11 20:14:34','Y',0),(39,NULL,NULL,'CM','CMLD',NULL,NULL,'AGENT1',NULL,'AGENT1','2019-02-11 20:14:41','Y',0),(40,NULL,NULL,'SQ','SQND',NULL,NULL,'AGENT1',NULL,'AGENT1','2019-02-11 20:14:46','Y',0),(43,NULL,NULL,'SU','SQUH',NULL,NULL,'AGENT1',NULL,'AGENT1','2019-03-14 09:35:58','Y',0),(1049,NULL,NULL,'DC','DCCD',NULL,NULL,'AGENT1',NULL,NULL,NULL,'Y',0),(1053,NULL,'011','','BTN',NULL,NULL,NULL,NULL,NULL,NULL,'Y',0);

#
# Structure for table "mstatus"
#

DROP TABLE IF EXISTS `mstatus`;
CREATE TABLE `mstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) DEFAULT NULL,
  `lblStatus` varchar(150) DEFAULT NULL,
  `Urutan` int(11) DEFAULT NULL,
  `NA` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "mstatus"
#

INSERT INTO `mstatus` VALUES (1,'Open','Open',1,'Y'),(2,'Pending','Pending',2,'Y'),(3,'Progress','Progress',3,'Y'),(4,'Closed','Closed',4,'Y');

#
# Structure for table "msuser"
#

DROP TABLE IF EXISTS `msuser`;
CREATE TABLE `msuser` (
  `userid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leveluser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `SMS` tinyint(3) DEFAULT NULL,
  `EMAIL` tinyint(4) DEFAULT NULL,
  `FACEBOOK` tinyint(4) DEFAULT NULL,
  `TWITTER` tinyint(4) DEFAULT NULL,
  `CHAT` tinyint(4) DEFAULT NULL,
  `INSTAGRAM` tinyint(4) DEFAULT NULL,
  `WHATSAPP` tinyint(4) DEFAULT NULL,
  `SMSBLAST` tinyint(4) DEFAULT NULL,
  `LOGIN` int(4) DEFAULT NULL,
  `IdAUX` int(4) DEFAULT NULL,
  `UNITKERJA` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `ORGANIZATION` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `OUTBOUND` tinyint(4) DEFAULT NULL,
  `INBOUND` tinyint(4) DEFAULT NULL,
  `MAX_CHAT` int(11) DEFAULT NULL,
  `MAX_QUEUE` int(11) DEFAULT NULL,
  `MAX_OUTBOUND` int(11) DEFAULT NULL,
  `MAX_EMAIL` int(11) DEFAULT NULL,
  `MAX_SMS` decimal(4,0) DEFAULT NULL,
  `MAX_CONCURRENT` int(11) DEFAULT NULL,
  `Agent_SignalR` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`userid`,`username`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "msuser"
#

INSERT INTO `msuser` VALUES (6,'jono','jono','jono@gmail.com','$2y$10$Bi6C11eEelaRyBQE2bvTSuTSu1C2mmEWoZT9Kbu49nK/mdo3/dMkS','layer1',NULL,NULL,'2021-07-02 05:22:23','2021-07-02 05:22:23',0,0,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,0,0,0,0,0,NULL),(7,'dudu','dudu','dudu@mail.com','$2y$10$5xk5YHG.r3FNaBEcjrGEF.zoBUat6rmy7RE.RWnAi6sQR4nSJyi9S','layer1',NULL,NULL,'2021-07-02 05:24:34','2021-07-02 05:24:34',0,0,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,0,0,0,0,0,NULL),(8,'rizki malik','malik','rizkinexx@gmail.com','$2y$10$BrTr6eAQUOLwV03wrptjWuIdPHaOaPBlOyrv5dHZ.Ip.fa5xUJCy6','layer1',NULL,NULL,'2021-07-02 05:52:53','2021-07-02 05:52:53',0,0,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,0,0,0,0,0,NULL),(9,'yoyo','yoyo','yoyo@gmail.com','$2y$10$G4q1yODfOOh82jU0NeIzr.cPXFSFH//Pjl7TKYdpedEa23r3MvSX2','layer1',NULL,NULL,'2021-07-02 06:02:56','2021-07-02 06:02:56',0,0,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,0,0,0,0,0,NULL),(11,'agent1','agent1','agent1@gmail.com','$2y$10$yi2GSKqvUhDODNyqN4VCGufszthcoRdRCw60dnaBn51Fb1dUrUhVm','layer1',NULL,'2fcmENShpthbQ9q5cH06dELxii2wEZmF6zBNC0FOWlagJ4vySb2yNdKNfBHe','2021-07-02 06:29:27','2021-07-02 06:29:27',1,0,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,0,0,0,0,0,NULL),(12,'agent100','agent100','agent100@gmail.com','$2y$10$yi2GSKqvUhDODNyqN4VCGufszthcoRdRCw60dnaBn51Fb1dUrUhVm','agent',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'agent200','agent200','agent200@gmail.com','$2a$10$WfmXE3V7leXVTxUhZR1ax.sQsiZKM3SgoWPf5yPxEOALv5CI5vAea','agent',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'Pa$$w0rd!','Pa$$w0rd!','Pa$$w0rd!','Pa$$w0rd!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

#
# Structure for table "msuser2"
#

DROP TABLE IF EXISTS `msuser2`;
CREATE TABLE `msuser2` (
  `userid` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `TS_EXT` varchar(50) DEFAULT NULL,
  `TS_NAME` varchar(50) DEFAULT NULL,
  `ACD` varchar(50) DEFAULT NULL,
  `AUTHORITY` varchar(50) DEFAULT NULL,
  `INCLUDE` char(3) GENERATED ALWAYS AS (0) STORED,
  `STATE` varchar(50) DEFAULT NULL,
  `IDENTIFIER` int(11) DEFAULT NULL,
  `ROLE` varchar(50) DEFAULT NULL,
  `VOICE_ROUTE` char(10) DEFAULT NULL,
  `EMAIL_ROUTE` char(10) DEFAULT NULL,
  `LINKSTATE` char(3) DEFAULT NULL,
  `PBX_LOIN_ID` varchar(50) DEFAULT NULL,
  `PBX_EXT` varchar(50) DEFAULT NULL,
  `ExpDate` date DEFAULT NULL,
  `PassTemp` varchar(10) DEFAULT NULL,
  `Flag` varchar(50) DEFAULT NULL,
  `ORGANIZATION` varchar(50) DEFAULT NULL,
  `INBOUND` tinyint(4) DEFAULT NULL,
  `OUTBOUND` tinyint(4) DEFAULT NULL,
  `FAX` tinyint(4) DEFAULT NULL,
  `SMS` tinyint(4) DEFAULT NULL,
  `EMAIL` tinyint(4) DEFAULT NULL,
  `FACEBOOK` tinyint(4) DEFAULT NULL,
  `TWITTER` tinyint(4) DEFAULT NULL,
  `ADMINTOOL` tinyint(4) DEFAULT NULL,
  `SMSBLAST` tinyint(4) DEFAULT NULL,
  `CHAT` tinyint(4) DEFAULT NULL,
  `SOSMED` tinyint(4) DEFAULT NULL,
  `HANDLE_CHAT` int(11) DEFAULT NULL,
  `ACTIVE` int(11) DEFAULT NULL,
  `ACTIVE_CHAT` int(11) DEFAULT NULL,
  `LEVELUSER` varchar(50) DEFAULT NULL,
  `NIK` varchar(50) DEFAULT NULL,
  `DATE_STAM` datetime DEFAULT NULL,
  `UNITKERJA` varchar(50) DEFAULT NULL,
  `USERCREATE` text,
  `NA` varchar(50) DEFAULT NULL,
  `WHATSAPP` tinyint(4) DEFAULT NULL,
  `LOGIN` int(11) DEFAULT NULL,
  `PATH` text,
  `IdAUX` varchar(4) DEFAULT NULL,
  `DescAUX` varchar(150) DEFAULT NULL,
  `MAX_CHAT` int(11) DEFAULT NULL,
  `MAX_QUEUE` int(11) DEFAULT NULL,
  `MAX_OUTBOUND` int(11) DEFAULT NULL,
  `MAX_EMAIL` int(11) DEFAULT NULL,
  `MAX_SMS` int(11) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(250) DEFAULT NULL,
  `GROUP_CAMPAIGN` varchar(100) DEFAULT NULL,
  `Agent_signalR` varchar(250) DEFAULT NULL,
  `MAX_CONCURRENT` int(11) DEFAULT NULL,
  `INSTAGRAM` tinyint(4) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userid`,`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=254 DEFAULT CHARSET=latin1;

#
# Data for table "msuser2"
#

INSERT INTO `msuser2` (`userid`,`username`,`name`,`password`,`TS_EXT`,`TS_NAME`,`ACD`,`AUTHORITY`,`STATE`,`IDENTIFIER`,`ROLE`,`VOICE_ROUTE`,`EMAIL_ROUTE`,`LINKSTATE`,`PBX_LOIN_ID`,`PBX_EXT`,`ExpDate`,`PassTemp`,`Flag`,`ORGANIZATION`,`INBOUND`,`OUTBOUND`,`FAX`,`SMS`,`EMAIL`,`FACEBOOK`,`TWITTER`,`ADMINTOOL`,`SMSBLAST`,`CHAT`,`SOSMED`,`HANDLE_CHAT`,`ACTIVE`,`ACTIVE_CHAT`,`LEVELUSER`,`NIK`,`DATE_STAM`,`UNITKERJA`,`USERCREATE`,`NA`,`WHATSAPP`,`LOGIN`,`PATH`,`IdAUX`,`DescAUX`,`MAX_CHAT`,`MAX_QUEUE`,`MAX_OUTBOUND`,`MAX_EMAIL`,`MAX_SMS`,`EMAIL_ADDRESS`,`GROUP_CAMPAIGN`,`Agent_signalR`,`MAX_CONCURRENT`,`INSTAGRAM`,`email_verified_at`,`remember_token`,`created_at`,`updated_at`) VALUES (1,'Achmad_Latif','ACHMAD ABDUL LATIF','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,1,1,NULL,NULL,1,0,NULL,NULL,NULL,'Agent','ACHMAD ABDUL LATIF',NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',2,3,15,3,NULL,NULL,'1','1233ac7a-2bf1-4abd-949a-db834f580bcc',2,1,NULL,NULL,NULL,NULL),(100,'CaseUnit','CaseUnit','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'CaseUnit','Hasan Basri',NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',2,NULL,5,NULL,NULL,'mubarok@invision-ap.com',NULL,NULL,1,0,NULL,NULL,NULL,NULL),(135,'ARIF024371','Arif Noviyanto','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(136,'ARNELIA021350','Arnelia Anggieta Tirana','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(137,'ATIKAH024356','Atikah Intan Kumala Sari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(138,'BELLA025376','Bella Agus Fatimah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(139,'BRILLYAN020894','Brillyan Dimas Prasetyo','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(140,'CAMELIA024360','Camelia Nurul Adhariyah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(141,'DEDI021355','Dedi Satrio','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,1,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,100,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(142,'DESI024367','Desi Saron Saraswati Hidayat','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(143,'DEWI023980','Dewi Ira Puspita Sari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(144,'DIMAS025377','Dimas Satrio','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(145,'ENDAH020907','Endah Dwi Lestari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(146,'FAIKA023061','Faika Sandy Yudha','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(147,'FAJAR025381','Fajar Agus Tri Fidyanto','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(148,'FATMA020881','Fatma Firmandari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,0,NULL,NULL,1,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,100,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(149,'FIARA021335','Fiara Diva Putri Wikana','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,0,NULL,NULL,1,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,100,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(150,'HANIFAH023981','Hanifah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(151,'IDA021363','Ida Rahayu Ningsih','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(152,'IKA020895','Ika Wiwin Wijayanti','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,1,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,100,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(153,'JERRY025378','Jerry Angga Saputra','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(154,'KARINA021825','Karina Tia Prastika','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(155,'KARUNIA020898','Karunia Putri Wijayanti','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(156,'LATIFAH024358','Latifah Nur Khoirun Nisa','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(157,'MEGA022516','Mega Salfia','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(158,'NUR022517','Nur Aini','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(159,'PUTRI020879','Putri Febrianti Eka Yoga','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(160,'PUTRI021826','Putri Intan Prastiwi','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(161,'RIZAL020904','Rizal Fauzi','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,1,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,100,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(162,'RURI020909','Ruri Cendhani','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'11',NULL,1,0,NULL,NULL,NULL,NULL),(163,'SETIYO024368','Setiyo Wahyu Wulandari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0a6fe868-8ef8-425f-843b-b975b3a1e1b3',1,0,NULL,NULL,NULL,NULL),(164,'UMI021378','Umi Haryani','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(165,'WIRAWAN020880','Wirawan Tera Wicaksana','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(166,'YOLA023060','Yola Lutfiananti','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(167,'ZSA020869','Zsa Zsa Wulan Permatasari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,1,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',100,NULL,NULL,100,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(168,'NOVI025383','Novi Bagus D Sukoco','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(169,'FATI025382','Fati Ar Rumni','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(170,'BHREY025931','Bhrey Asbias Baskoro Nugroho','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(171,'DESSY025928','Dessy Susanti','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(172,'WAHYU025929','Wahyu Setyo Nugroho','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(173,'ANDRI026923','Andri Septian Nugroho','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,0,0,NULL,NULL,1,1,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',100,NULL,NULL,NULL,NULL,NULL,'6','4364fde2-4414-43a3-b345-639c934409e1',1,0,NULL,NULL,NULL,NULL),(174,'GANTHI026925','Ganthi Wulandari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(175,'RISKA026924','Riska Avriliana','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(176,'RISKA026930','Riska Alfiani','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(177,'ANNISA026931','Annisa Sabar Cahyati','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(178,'ANDHIKA027661','Andhika Abryan Reviansyah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'6',NULL,1,0,NULL,NULL,NULL,NULL),(179,'VERLITA027663','Verlita Oppie Agyta','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(180,'ANDONO020866','Andono Swandaru','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(181,'BUNGA020906','Bunga Shahwah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(182,'DISTIA020885','Distia Ria Aryadi','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(183,'FITRIA020918','Fitria Widhiastuti','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(184,'HENDAYANA025385','Hendayana Sinabariba','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(185,'HERLAN020875','Herlan Syafitriyadi','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(186,'HERU019903','Heru Saputro','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(187,'INDRI020859','Indri Hapsari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(188,'ISNAIN020864','Isnain Al Husain P.A','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(189,'JEREMIA021395','Jeremia Sipahutar','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(190,'LILIS023977','Lilis Suryani','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(191,'MARDIANTO020919','Mardianto','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(192,'RATNA020861','Ratna Triana Wati','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(193,'SANDY020863','Sandy Asmara','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ab56d50e-6873-4baf-b443-3c53b688c22c',1,0,NULL,NULL,NULL,NULL),(194,'RISKY026926','Risky Tri Sulaksmi','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(195,'MUHAMMAD026929','Muhammad Farid Nurrohman','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(196,'Purwanti9970','Purwanti','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Administrator',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d423175b-fefc-4796-affb-8b691518e3e8',1,0,NULL,NULL,NULL,NULL),(200,'MICEND020876','Micend Widianingrum','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,'',NULL,'Y',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(201,'RIZAL','SAMSU RIZAL TEST','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'CaseUnit',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,'rizalsamsurizal708@gmail.com',NULL,'ad7d4bdb-f4d5-4470-8412-d923a18fe6aa',1,0,NULL,NULL,NULL,NULL),(203,'ISNAIN091810','ISNAIN91810','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',100,NULL,NULL,3,NULL,NULL,'6','caef0250-513c-440a-a908-d2fbc6947bc2',1,0,NULL,NULL,NULL,NULL),(204,'AJI021858','Aji Ardiansyah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'7',NULL,1,0,NULL,NULL,NULL,NULL),(205,'DESRI021865','Desri Mahdalena Damanik','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'4',NULL,1,0,NULL,NULL,NULL,NULL),(206,'SIYAT021854','Siyat Indra Saputra','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'N',NULL,0,NULL,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,'7',NULL,1,0,NULL,NULL,NULL,NULL),(207,'KOTRUNNIDA021857','Kotrunnida','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'10',NULL,1,0,NULL,NULL,NULL,NULL),(208,'YULI024989','Yuli Syafmawarni','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'9',NULL,1,0,NULL,NULL,NULL,NULL),(209,'MESNITA021867','Mesnita Simarmata','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'10',NULL,1,0,NULL,NULL,NULL,NULL),(210,'AGIE027694','Agie Nuarie','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'4',NULL,1,0,NULL,NULL,NULL,NULL),(211,'ANDRI027695','Andri Pahrizal','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'4',NULL,1,0,NULL,NULL,NULL,NULL),(212,'SRI021855','Sri Wahyuni','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'10',NULL,1,0,NULL,NULL,NULL,NULL),(213,'OTNIEL026942','Otniel Takalamingan','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'4',NULL,1,0,NULL,NULL,NULL,NULL),(214,'FISCA024995','Fisca Saptiyani Utami','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'10',NULL,1,0,NULL,NULL,NULL,NULL),(215,'NURBAITY027688','Nurbaity Lubis','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'N',NULL,0,NULL,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,'7',NULL,1,0,NULL,NULL,NULL,NULL),(216,'DJUWANA027690','Djuwana Mediyar','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'9',NULL,1,0,NULL,NULL,NULL,NULL),(217,'MOCHAMAD027689','Mochamad Mashur','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'4',NULL,1,0,NULL,NULL,NULL,NULL),(218,'RIVAI027691','Muhammad Rivai','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'N',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'7',NULL,1,0,NULL,NULL,NULL,NULL),(219,'NAOMI021868','Naomi Paulina Tobing','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'9',NULL,1,0,NULL,NULL,NULL,NULL),(220,'DALTON026941','Dalton Hermawan Sahadagi','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,'6',NULL,1,0,NULL,NULL,NULL,NULL),(221,'SYAHRUL027692','Syahrul Riduan','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'9',NULL,1,0,NULL,NULL,NULL,NULL),(222,'ABDUL027693','Abdul Rahman H','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'',NULL,'Y',NULL,0,NULL,'9','READY',1,NULL,1,1,NULL,NULL,'11',NULL,1,0,NULL,NULL,NULL,NULL),(223,'L2','L2','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'','4f24f954-2029-4a05-8404-41367ca9bc02',1,0,NULL,NULL,NULL,NULL),(224,'TAFANI021853','Tafani Fillah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(225,'AROVAH021871','Arovah Idham','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Administrator',NULL,NULL,'',NULL,'Y',NULL,1,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'c84233f1-b63e-44b1-85d2-d66391564196',1,0,NULL,NULL,NULL,NULL),(226,'CICI021851','Cici Nursanti Sihite','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Administrator',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(227,'DIAN021872','Dian Eka Pajawiyanti','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'fb52999a-8758-4e67-9def-b5e4f3e7cc95',1,0,NULL,NULL,NULL,NULL),(228,'IRMAWATI021863','Nur Irmawati','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'N',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(229,'AGUSTINA024991','Agustina Berliana','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(230,'ARYA021856','Arya Pramudita','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(231,'ALI021864','Ali Mustofa','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(232,'MUTHIA022513','Muthia Windarti','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(233,'YANTI021873','Yanti Veronika Nainggolan','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(234,'AFRINO021889','Afrino Azanova','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(235,'FIKRIYANSAH021860','Fikriyansah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(236,'FINA021852','Fina Listiani Hakim','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL),(237,'endyrayn','rayn endy','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Administrator',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',100,NULL,NULL,NULL,NULL,NULL,NULL,'e3dc19ff-760e-42ac-a2b3-7c956778067d',1,0,NULL,NULL,NULL,NULL),(238,'Yanova16177','Yanova','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,1,1,NULL,NULL,1,1,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',2,NULL,5,5,NULL,NULL,'6','6d601528-8b8a-415f-99e2-d5b363aafc15',2,1,NULL,NULL,NULL,NULL),(239,'Agent1','Agent1','$2y$10$yi2GSKqvUhDODNyqN4VCGufszthcoRdRCw60dnaBn51Fb1dUrUhVm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,1,1,NULL,NULL,1,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,1,NULL,'9','READY',100,NULL,NULL,NULL,NULL,NULL,'','1e8b3a74-45cd-410f-b77c-f42de33e04c8',100,1,'2021-07-08 19:53:48',NULL,NULL,NULL),(241,'invisionrizal','Samsu Rizal','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Administrator',NULL,NULL,NULL,NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,'rizal@invision-ap.com',NULL,NULL,1,0,NULL,NULL,NULL,NULL),(242,'YUSAN024986','YUSAN OVIE LATIF','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',1,1,NULL,NULL,0,0,0,NULL,NULL,1,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',100,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(243,'MUHAMMAD027691','Muhammad Rivai','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'10',NULL,1,0,NULL,NULL,NULL,NULL),(244,'NUR021863','Nur Irmawati','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Supervisor',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(245,'LITTA028415','Litta Brigita Tarigan','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'9',NULL,1,0,NULL,NULL,NULL,NULL),(246,'RINO028416','Rino Afriyaldi','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'9',NULL,1,0,NULL,NULL,NULL,NULL),(247,'DIVYA028417','Divya Rachmadanti Fauziah','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1053',0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'9',NULL,1,0,NULL,NULL,NULL,NULL),(248,'ISTI28460','Isti Ridyawati','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(249,'HESTI28461','Hesti fatmalasari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(250,'LILIS28462','Lilis Setyowati','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(251,'DWI28463','Dwi Putri Setyowati','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(252,'RUDI00028520','Rudi Fauzi Al Mustari','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,1,0,NULL,NULL,NULL,NULL),(253,'ADE00028521','Ade Eman Budiman','login123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,NULL,NULL,0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,'Agent',NULL,NULL,'1',NULL,'Y',NULL,0,NULL,'9','READY',NULL,NULL,150,NULL,NULL,NULL,'7',NULL,1,0,NULL,NULL,NULL,NULL);

#
# Structure for table "password_resets"
#

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "password_resets"
#


#
# Structure for table "products"
#

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "products"
#

INSERT INTO `products` VALUES (1,'TV',12000.00,NULL,NULL),(2,'HP Iphone',14000.00,NULL,'2021-07-31 17:27:58'),(3,'Laptop',45000.00,NULL,NULL),(5,'PC 1',90000.00,NULL,'2021-07-31 17:40:44'),(7,'legion',200000.00,NULL,'2021-07-31 17:34:44');

#
# Structure for table "sessions"
#

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "sessions"
#

INSERT INTO `sessions` VALUES ('LLpiXuKUFTz6gbdkEjCZwLKROHEwh9HC7jV9cj2b',NULL,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiZ0Q2VGRrWjZsaDBEaGJaWDBIb282SnVydnhtejA0Y0Z6QXRMZkFZaiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMCI7fX0=',1625323887),('Rk6i0KyTYMKYxx6rZJw6GLAQ1NVfK4l6JR7ShT4s',NULL,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiTWtVN2t4MHlYaXlzcDB0RmlEY2NZN0NoQkFWREpRNEtTRDkxOFJKaiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjc6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9sb2dpbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=',1625321022),('T4fwzO1jVp4HClQxUiD8aLFXm6UOL8E0cc6cD0Cv',NULL,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiNERGTTR2aGhhaU5lSkV4MUM1Q2dBMXdzTlI2SjlJMEVXczVMU0RGbSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMCI7fX0=',1625328505);

#
# Structure for table "tchat"
#

DROP TABLE IF EXISTS `tchat`;
CREATE TABLE `tchat` (
  `TrxChatID` int(18) NOT NULL AUTO_INCREMENT,
  `ChatID` varchar(255) DEFAULT NULL,
  `UserID` varchar(255) DEFAULT NULL,
  `TicketNumber` varchar(255) DEFAULT NULL,
  `GroupID` varchar(255) DEFAULT NULL,
  `CompID` varchar(255) DEFAULT NULL,
  `OrganizationID` varchar(255) DEFAULT NULL,
  `CustomerID` varchar(255) DEFAULT NULL,
  `Nama` varchar(255) DEFAULT NULL,
  `FlagTo` varchar(255) DEFAULT NULL,
  `Pesan` longtext,
  `DateCreate` datetime DEFAULT NULL,
  `StatusChat` varchar(255) DEFAULT NULL,
  `RoomID` varchar(255) DEFAULT NULL,
  `AssignTo` varchar(255) DEFAULT NULL,
  `AssignStatus` varchar(255) DEFAULT NULL,
  `AlamatIP` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `FlagEnd` varchar(255) DEFAULT NULL,
  `FlagEndUser` varchar(255) DEFAULT NULL,
  `FlagNotif` varchar(255) DEFAULT NULL,
  `FlagTicket` varchar(255) DEFAULT NULL,
  `BlinkChat` varchar(255) DEFAULT NULL,
  `agent_handle` varchar(255) DEFAULT NULL,
  `agent_chat` varchar(255) DEFAULT NULL,
  `CNT` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `URLSurvey` varchar(255) DEFAULT NULL,
  `FGreeting` varchar(255) DEFAULT NULL,
  `TextJoint` varchar(255) DEFAULT NULL,
  `IDJenisChat` varchar(255) DEFAULT NULL,
  `JenisChat` varchar(255) DEFAULT NULL,
  `DateAssign` datetime DEFAULT NULL,
  `UserLogin` varchar(255) DEFAULT NULL,
  `imgbase` longtext,
  `Filename` longtext,
  `AskJoni` varchar(255) DEFAULT NULL,
  `flagRead` varchar(255) DEFAULT NULL,
  `mVendor` varchar(255) DEFAULT NULL,
  `typeFile` varchar(255) DEFAULT NULL,
  `Token` varchar(255) DEFAULT NULL,
  `flagRead2` varchar(255) DEFAULT NULL,
  `flagRead3` varchar(255) DEFAULT NULL,
  `FlagEndWebArqSuk` varchar(255) DEFAULT NULL,
  `EndchatUser` varchar(255) DEFAULT NULL,
  `statusCount` varchar(255) DEFAULT NULL,
  `Agent_ID` varchar(255) DEFAULT NULL,
  `wa_media_type` varchar(50) DEFAULT NULL,
  `wa_media_caption` varchar(50) DEFAULT NULL,
  `wa_media_url` varchar(150) DEFAULT NULL,
  `wa_media_longitude` varchar(50) DEFAULT NULL,
  `wa_media_latitude` varchar(50) DEFAULT NULL,
  `TypeChannel` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`TrxChatID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tchat"
#


#
# Structure for table "tchat_bot"
#

DROP TABLE IF EXISTS `tchat_bot`;
CREATE TABLE `tchat_bot` (
  `TrxChatID` int(11) NOT NULL AUTO_INCREMENT,
  `ChatID` varchar(255) DEFAULT NULL,
  `UserID` varchar(255) DEFAULT NULL,
  `TicketNumber` varchar(255) DEFAULT NULL,
  `GroupID` varchar(255) DEFAULT NULL,
  `CompID` varchar(255) DEFAULT NULL,
  `OrganizationID` varchar(255) DEFAULT NULL,
  `CustomerID` varchar(255) DEFAULT NULL,
  `Nama` varchar(255) DEFAULT NULL,
  `FlagTo` varchar(255) DEFAULT NULL,
  `Pesan` longtext,
  `DateCreate` datetime DEFAULT NULL,
  `StatusChat` varchar(255) DEFAULT NULL,
  `RoomID` varchar(255) DEFAULT NULL,
  `AssignTo` varchar(255) DEFAULT NULL,
  `AssignStatus` varchar(255) DEFAULT NULL,
  `AlamatIP` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `FlagEnd` varchar(255) DEFAULT NULL,
  `FlagEndUser` varchar(255) DEFAULT NULL,
  `FlagNotif` varchar(255) DEFAULT NULL,
  `FlagTicket` varchar(255) DEFAULT NULL,
  `BlinkChat` varchar(255) DEFAULT NULL,
  `agent_handle` varchar(255) DEFAULT NULL,
  `agent_chat` varchar(255) DEFAULT NULL,
  `CNT` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `URLSurvey` varchar(255) DEFAULT NULL,
  `FGreeting` varchar(255) DEFAULT NULL,
  `TextJoint` varchar(255) DEFAULT NULL,
  `IDJenisChat` varchar(255) DEFAULT NULL,
  `JenisChat` varchar(255) DEFAULT NULL,
  `DateAssign` varchar(255) DEFAULT NULL,
  `UserLogin` varchar(255) DEFAULT NULL,
  `imgbase` longtext,
  `Filename` longtext,
  `AskJoni` varchar(255) DEFAULT NULL,
  `flagRead` varchar(255) DEFAULT NULL,
  `mVendor` varchar(255) DEFAULT NULL,
  `typeFile` varchar(255) DEFAULT NULL,
  `Token` varchar(255) DEFAULT NULL,
  `flagRead2` varchar(255) DEFAULT NULL,
  `flagRead3` varchar(255) DEFAULT NULL,
  `FlagEndWebArqSuk` varchar(255) DEFAULT NULL,
  `EndchatUser` varchar(255) DEFAULT NULL,
  `statusCount` varchar(255) DEFAULT NULL,
  `Agent_ID` varchar(255) DEFAULT NULL,
  `wa_media_type` varchar(50) DEFAULT NULL,
  `wa_media_caption` varchar(50) DEFAULT NULL,
  `wa_media_url` varchar(150) DEFAULT NULL,
  `wa_media_longitude` varchar(50) DEFAULT NULL,
  `wa_media_latitude` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`TrxChatID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tchat_bot"
#


#
# Structure for table "tchat_botend"
#

DROP TABLE IF EXISTS `tchat_botend`;
CREATE TABLE `tchat_botend` (
  `TrxChatID` int(11) NOT NULL,
  `ChatID` varchar(255) DEFAULT NULL,
  `UserID` varchar(255) DEFAULT NULL,
  `TicketNumber` varchar(255) DEFAULT NULL,
  `GroupID` varchar(255) DEFAULT NULL,
  `CompID` varchar(255) DEFAULT NULL,
  `OrganizationID` varchar(255) DEFAULT NULL,
  `CustomerID` varchar(255) DEFAULT NULL,
  `Nama` varchar(255) DEFAULT NULL,
  `FlagTo` varchar(255) DEFAULT NULL,
  `Pesan` longtext,
  `DateCreate` datetime DEFAULT NULL,
  `StatusChat` varchar(255) DEFAULT NULL,
  `RoomID` varchar(255) DEFAULT NULL,
  `AssignTo` varchar(255) DEFAULT NULL,
  `AssignStatus` varchar(255) DEFAULT NULL,
  `AlamatIP` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `FlagEnd` varchar(255) DEFAULT NULL,
  `FlagEndUser` varchar(255) DEFAULT NULL,
  `FlagNotif` varchar(255) DEFAULT NULL,
  `FlagTicket` varchar(255) DEFAULT NULL,
  `BlinkChat` varchar(255) DEFAULT NULL,
  `agent_handle` varchar(255) DEFAULT NULL,
  `agent_chat` varchar(255) DEFAULT NULL,
  `CNT` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `URLSurvey` varchar(255) DEFAULT NULL,
  `FGreeting` varchar(255) DEFAULT NULL,
  `TextJoint` varchar(255) DEFAULT NULL,
  `IDJenisChat` varchar(255) DEFAULT NULL,
  `JenisChat` varchar(255) DEFAULT NULL,
  `DateAssign` varchar(255) DEFAULT NULL,
  `UserLogin` varchar(255) DEFAULT NULL,
  `imgbase` longtext,
  `Filename` longtext,
  `AskJoni` varchar(255) DEFAULT NULL,
  `flagRead` varchar(255) DEFAULT NULL,
  `mVendor` varchar(255) DEFAULT NULL,
  `typeFile` varchar(255) DEFAULT NULL,
  `Token` varchar(255) DEFAULT NULL,
  `flagRead2` varchar(255) DEFAULT NULL,
  `flagRead3` varchar(255) DEFAULT NULL,
  `FlagEndWebArqSuk` varchar(255) DEFAULT NULL,
  `EndchatUser` varchar(255) DEFAULT NULL,
  `statusCount` varchar(255) DEFAULT NULL,
  `Agent_ID` varchar(255) DEFAULT NULL,
  `wa_media_type` varchar(50) DEFAULT NULL,
  `wa_media_caption` varchar(50) DEFAULT NULL,
  `wa_media_url` varchar(150) DEFAULT NULL,
  `wa_media_longitude` varchar(50) DEFAULT NULL,
  `wa_media_latitude` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tchat_botend"
#


#
# Structure for table "tchat_end"
#

DROP TABLE IF EXISTS `tchat_end`;
CREATE TABLE `tchat_end` (
  `TrxChatID` int(18) NOT NULL,
  `ChatID` varchar(255) DEFAULT NULL,
  `UserID` varchar(255) DEFAULT NULL,
  `TicketNumber` varchar(255) DEFAULT NULL,
  `GroupID` varchar(255) DEFAULT NULL,
  `CompID` varchar(255) DEFAULT NULL,
  `OrganizationID` varchar(255) DEFAULT NULL,
  `CustomerID` varchar(255) DEFAULT NULL,
  `Nama` varchar(255) DEFAULT NULL,
  `FlagTo` varchar(255) DEFAULT NULL,
  `Pesan` longtext,
  `DateCreate` datetime DEFAULT NULL,
  `StatusChat` varchar(255) DEFAULT NULL,
  `RoomID` varchar(255) DEFAULT NULL,
  `AssignTo` varchar(255) DEFAULT NULL,
  `AssignStatus` varchar(255) DEFAULT NULL,
  `AlamatIP` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `FlagEnd` varchar(255) DEFAULT NULL,
  `FlagEndUser` varchar(255) DEFAULT NULL,
  `FlagNotif` varchar(255) DEFAULT NULL,
  `FlagTicket` varchar(255) DEFAULT NULL,
  `BlinkChat` varchar(255) DEFAULT NULL,
  `agent_handle` varchar(255) DEFAULT NULL,
  `agent_chat` varchar(255) DEFAULT NULL,
  `CNT` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `URLSurvey` varchar(255) DEFAULT NULL,
  `FGreeting` varchar(255) DEFAULT NULL,
  `TextJoint` varchar(255) DEFAULT NULL,
  `IDJenisChat` varchar(255) DEFAULT NULL,
  `JenisChat` varchar(255) DEFAULT NULL,
  `DateAssign` varchar(255) DEFAULT NULL,
  `UserLogin` varchar(255) DEFAULT NULL,
  `imgbase` longtext,
  `Filename` longtext,
  `AskJoni` varchar(255) DEFAULT NULL,
  `flagRead` varchar(255) DEFAULT NULL,
  `mVendor` varchar(255) DEFAULT NULL,
  `typeFile` varchar(255) DEFAULT NULL,
  `Token` varchar(255) DEFAULT NULL,
  `flagRead2` varchar(255) DEFAULT NULL,
  `flagRead3` varchar(255) DEFAULT NULL,
  `FlagEndWebArqSuk` varchar(255) DEFAULT NULL,
  `EndchatUser` varchar(255) DEFAULT NULL,
  `statusCount` varchar(255) DEFAULT NULL,
  `Agent_ID` varchar(255) DEFAULT NULL,
  `wa_media_type` varchar(50) DEFAULT NULL,
  `wa_media_caption` varchar(50) DEFAULT NULL,
  `wa_media_url` varchar(150) DEFAULT NULL,
  `wa_media_longitude` varchar(50) DEFAULT NULL,
  `wa_media_latitude` varchar(50) DEFAULT NULL,
  `TypeChannel` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tchat_end"
#


#
# Structure for table "tsave_token"
#

DROP TABLE IF EXISTS `tsave_token`;
CREATE TABLE `tsave_token` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `Page_Name` text,
  `Page_ID` varchar(250) DEFAULT NULL,
  `Page_Token` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30051 DEFAULT CHARSET=latin1;

#
# Data for table "tsave_token"
#

INSERT INTO `tsave_token` VALUES (30035,'Invision Astrindo Pratama','374215903210349','EAAHB4P6XPywBADlUcXnuabnYOx4mYKm4tu9cLGk8u6C5KkY1nRmnJdeUIQryEWc4IBL5WdDpP0xSUzhmvhcQpFvehxWOuTLH4IZBSAHMW6EPewDHlQq03Bg81eBSWdcikcdGUSZC7NgBJh2z8TDNKTynJScaCFfC9BVZCvRsYZCuzrrqfJPOvzajZB9tMAwcHhmpATZCsuFQZDZD'),(30043,'Mendawai','109930817498267','EAAHB4P6XPywBAGMPY0v7baASZAPBfRHs9hyYVyG7rZBQZBIIUoJP3vgnDp7EF9KS9T779XqIlS4qMRXkNBr7nmU4YEgbWGpCna4tqP82CA7WFigjFx9s5V5cayWzKY0oCkGOmhC0iNLjqANMAl0EbCUBPxqkgf2rr3bPn8UvttbecGe2N0VP2bZCJa9sKHYZCEOj3MzaiHwZDZD'),(30044,'Smart Office','259241734726445','EAAHB4P6XPywBAEyZALvtAYDgKgdCcYDXI22kSLnDnGxD7c6GZCRFseZBfnRkswHiONGFZChVC13qi7ZBofMCNmfk2FZCOuesXgeQPbb5o4jPtmXsG0MkgEJ3sfnoSCVOGYZBCZBXamtUwytQDdtU1IgkX32h5PNyM7a54zHZBF9YSFQqNE7Sh8qcMGijR1mfVUb4P5310XPZBIHgZDZD'),(30045,'Leedar','459413277722973','EAAHB4P6XPywBACsBgtioGv7G2PmAsn9herOgtFlNXAs1zJNm9f7prsLBv1rgbhCVdJaUkZBvgDlXhQisCibfYmZAVICvjiibJZCMo4iZBPWy3titid2tZBx6I4Khsne5mPPT7nJF0W7W5kjVkZB13gEjAZA3SVEqot39e1zZCmzrtvR8mUBiK5YkU4ZAzavmNjLDahW0ACLapOwZDZD'),(30046,'Riuke','106034077823092','EAAHB4P6XPywBAI6Sn70uSaF0zaDJrad57SJyZBv1e0nDWrlCn3kScZAvhWex2m6KWRsrDdZAuL7bkpGgdwGqniZBUGTkMCLSIWpVzNEHlu5Jjh88ZCmyM2qVNP8CUncZBkWTZAt7vWfsLQj9TG8QoLqxKaStC5mgIUIg8AY1toVnHi6mXZC0tZBM2VwjQeebkZA2YZD'),(30047,'Fitria Fashion','1983071568641760','EAAHB4P6XPywBAJ6ecyhBnXAp7ZBtdrlmWLvVvQtuiasSKKmUvnOMZAiXyiuZCkPQfKRZAXxUi4CiGZC3FAJDnTkoLZCcXxArhtqtkh104ECKWlQBbFiUwJVIhCCeufxOOeIeHu9NQhlcnWMo9IZAtXoMmoo1ZA97o6bM6OkoAln7SPsM7EGPf3ZBeZBDXUspSApO0ZD'),(30049,'MNH Fashion','100962628296701','EAAHB4P6XPywBAEdpKWHgeviOmX4iH14bqoi4Moy4jOgnWeA9QpS13JivJh8YiK2HHHQ9E9GGxNodPCwHyDhebqlhvg3ZC9SlCTEbVhNIpDpxfEGZAQ0o2LkRbQFcSWeYZArzlLPOwuZB54HVjPU4x4aFziZC436lUXRXDaZCRLgc9rsbWTEHPO'),(30050,'Raisya Fashion','103800374694004','EAAHB4P6XPywBAKWVY26CsL8hIkOFJORpSWePAuRGZBz2gzfKgArZAstoj21vnQeC5ejSxMwBE2CfUtlDC6QoMiC1WnpyRpRadhJtYipxAqSIPopEGKAqL7SjRFd3QAhr4UcpHSvsXDuK2EG4zPnIDMm9V1k9A2HP2QkCug1u64qBu9MrQC');

#
# Structure for table "tsave_token_ig"
#

DROP TABLE IF EXISTS `tsave_token_ig`;
CREATE TABLE `tsave_token_ig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instagramid` varchar(50) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `page_id` text,
  `page_name` text,
  `access_token` longtext,
  `user_token` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2038 DEFAULT CHARSET=latin1;

#
# Data for table "tsave_token_ig"
#

INSERT INTO `tsave_token_ig` VALUES (2037,'17841426077494261','invisionapratama','374215903210349','Invision Astrindo Pratama','EAAHB4P6XPywBAJo4H24KiEjvbp6yZBXW0i72C0htpVM3LK4jz6dpKeqdJ4fXcytqnjbtMgZBVNmVGZAowqvkiY22vG6dnWeekC1Dj131FZB5uBOYpZBSoMTKLW4xOi8LFz5GOC1PP2ALKfDyqJadqIGO8hE4HAeXJIEbyluU4hzPrp5tT97P9RrlZCF7C3IjIZD','EAAHB4P6XPywBAN3yCFSF9ZB1ZAhBjp8eGBxzo3sXN9aSrx37YypZAiElwsqpNhPTMW5mQZBnrMlSdE4ZA2hORjIOvLGbZAMyXq4UxmaPbZCuqQck7d14RZAhXbdEQaHO9RPQl8KWVi3wApiTIxn2uN0dgTn9THhcoT8wovGDPdlbRar3XbpXL2TH');

#
# Structure for table "tsave_token_twitter"
#

DROP TABLE IF EXISTS `tsave_token_twitter`;
CREATE TABLE `tsave_token_twitter` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(150) DEFAULT NULL,
  `screen_name` varchar(150) DEFAULT NULL,
  `oauth_token` text,
  `oauth_token_secret` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Data for table "tsave_token_twitter"
#

INSERT INTO `tsave_token_twitter` VALUES (7,'1176382758257512448','InvisionAP','1176382758257512448-v6UP4nf9Tkh3uLN03R8eRtl2CFhhNi','xWDWdVq1kZFEk5rt0AJ2RuZqGDd9p1YPqDxgAy4hShgA7');

#
# Structure for table "tticket"
#

DROP TABLE IF EXISTS `tticket`;
CREATE TABLE `tticket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NIK` varchar(50) DEFAULT NULL,
  `TicketNumber` varchar(50) NOT NULL,
  `GroupTicketNumber` varchar(50) DEFAULT NULL,
  `Channel_Code` varchar(5) DEFAULT NULL,
  `UnitID` varchar(150) DEFAULT NULL,
  `TicketSource` varchar(20) DEFAULT NULL,
  `TicketSourceName` text,
  `TicketGroup` varchar(20) DEFAULT NULL,
  `TicketGroupName` text,
  `ComplaintLevel` varchar(20) DEFAULT NULL,
  `CategoryID` varchar(50) DEFAULT NULL,
  `CategoryName` text,
  `SubCategory1ID` varchar(50) DEFAULT NULL,
  `SubCategory1Name` varchar(250) DEFAULT NULL,
  `SubCategory2ID` varchar(50) DEFAULT NULL,
  `SubCategory2Name` varchar(250) DEFAULT NULL,
  `SubCategory3ID` varchar(50) DEFAULT NULL,
  `SubCategory3Name` varchar(250) DEFAULT NULL,
  `DetailComplaint` longtext,
  `ResponComplaint` longtext,
  `DateAgentResponse` datetime DEFAULT NULL,
  `SLAResponseAgent` int(11) DEFAULT NULL,
  `SLA` int(11) DEFAULT NULL,
  `Severity` varchar(50) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `UserCreate` varchar(50) DEFAULT NULL,
  `DateCreate` datetime DEFAULT NULL,
  `UserClose` varchar(50) DEFAULT NULL,
  `DateClose` datetime DEFAULT NULL,
  `TicketPosition` varchar(20) DEFAULT NULL,
  `ClosedBy` varchar(2) DEFAULT NULL,
  `KirimEmail` varchar(10) DEFAULT NULL,
  `KirimEmailLayer` varchar(10) DEFAULT NULL,
  `NA` varchar(1) DEFAULT NULL,
  `DateCreateReal` datetime DEFAULT NULL,
  `OverClockSystem` varchar(50) DEFAULT NULL,
  `Dispatch_user` text,
  `Dispatch_tgl` datetime DEFAULT NULL,
  `Divisi` varchar(50) DEFAULT NULL,
  `Dispatch_divisi_tgl` datetime DEFAULT NULL,
  `Attch` longtext,
  `Posting` varchar(50) DEFAULT NULL,
  `IdTabel` varchar(200) DEFAULT NULL,
  `GroupID` varchar(50) DEFAULT NULL,
  `CompID` varchar(50) DEFAULT NULL,
  `OrganizationID` varchar(50) DEFAULT NULL,
  `channelid` varchar(250) DEFAULT NULL,
  `NAMA_PELAPOR` varchar(250) DEFAULT NULL,
  `EMAIL_PELAPOR` varchar(250) DEFAULT NULL,
  `PHONE_PELAPOR` varchar(250) DEFAULT NULL,
  `ALAMAT_PELAPOR` varchar(250) DEFAULT NULL,
  `FLAG_GROUP_TICKET` varchar(5) DEFAULT NULL,
  `AccountInbound` varchar(100) DEFAULT NULL,
  `NomorRekening` varchar(100) DEFAULT NULL,
  `SkalaPrioritas` varchar(100) DEFAULT NULL,
  `JenisNasabah` varchar(100) DEFAULT NULL,
  `IDLevel3` varchar(100) DEFAULT NULL,
  `Phone` varchar(50) DEFAULT NULL,
  `SumberInformasi` varchar(150) DEFAULT NULL,
  `FlagChannel` varchar(100) DEFAULT NULL,
  `TypeChannel` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`,`TicketNumber`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tticket"
#


#
# Structure for table "tticketsatus"
#

DROP TABLE IF EXISTS `tticketsatus`;
CREATE TABLE `tticketsatus` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TicketStatus` varchar(50) DEFAULT NULL,
  `Status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tticketsatus"
#


#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (2,'red bionic','redbionic','red@gmail.com',NULL,'$2y$10$a4FtlA3POI8Z.v7n9uvF9ezPMdcTZrRRb9bzZOafurWcjd9bGL2sW','t5swnQESs0jEJxChctUJiqUdoFe8g1FOUQzyCFkVMd0sLjgArewKxJ1tR9pM','2021-06-30 10:30:34','2021-06-30 10:30:34'),(3,'malik','malik','malik@gmail.com',NULL,'$2y$10$b/.PEVX6.5DuHT.3jdjpC.jyavi6oeNBOSVaHreLvqNbkkvDu9/VW',NULL,'2021-07-01 13:41:18','2021-07-01 13:41:18'),(4,'anto','anto',NULL,NULL,'$2y$10$ox52dk7141tTeYr1gljwvuKoHiaquFIxGg0uiCenaFHZ9bMsWSIpa',NULL,'2021-07-01 13:43:17','2021-07-01 13:43:17'),(5,'dede','dede','dede@gmail.com',NULL,'samsung9080',NULL,'2021-07-01 13:44:33','2021-07-01 13:44:33');

#
# View "view_menu"
#

DROP VIEW IF EXISTS `view_menu`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `view_menu`
  AS
SELECT
  `menu1`.`MenuID` AS 'ID',
  0 AS 'RootID',
  `menu1`.`MenuName`,
  `menu1`.`MenuID`,
  0 AS 'SubMenuID',
  0 AS 'SubMenuTree',
  `menu1`.`Url`,
  'Menu' AS 'Level'
FROM
  `menu1`
UNION
SELECT
  `menu2`.`SubMenuID` AS 'ID',
  `menu2`.`MenuID` AS 'RootID',
  `menu2`.`SubMenuName` AS 'MenuName',
  `menu2`.`MenuID`,
  `menu2`.`SubMenuID`,
  0 AS 'SubMenuTree',
  `menu2`.`Url`,
  'Modul' AS 'Level'
FROM
  `menu2`
UNION
SELECT
  `menu3`.`SubMenuIDTree` AS 'ID',
  `menu3`.`SubMenuID` AS 'RootID',
  `menu3`.`MenuTreeName` AS 'MenuName',
  `menu3`.`MenuID`,
  `menu3`.`SubMenuID`,
  `menu3`.`SubMenuIDTree`,
  `menu3`.`Url`,
  'SubModul' AS 'Level'
FROM
  `menu3`;

#
# View "view_token"
#

DROP VIEW IF EXISTS `view_token`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `view_token`
  AS
SELECT
  `listtoken`.`PageName`,
  `listtoken`.`PageID`,
  `listtoken`.`Channel`,
  `listtoken`.`AccountName`,
  `listtoken`.`AccountID`,
  `listtoken`.`PageToken`,
  `listtoken`.`PageTokenSecret`
FROM
  (SELECT
    `tsave_token`.`Page_Name` AS 'PageName',
    `tsave_token`.`Page_ID` AS 'PageID',
    'Facebook' AS 'Channel',
    '' AS 'AccountName',
    '' AS 'AccountID',
    `tsave_token`.`Page_Token` AS 'PageToken',
    '' AS 'PageTokenSecret'
  FROM
    `tsave_token`
  UNION
  SELECT
    `tsave_token_ig`.`page_name` AS 'PageName',
    `tsave_token_ig`.`instagramid` AS 'PageID',
    'Instagram' AS 'Channel',
    `tsave_token_ig`.`username` AS 'AccountName',
    `tsave_token_ig`.`instagramid` AS 'AccountID',
    `tsave_token_ig`.`access_token` AS 'PageToken',
    '' AS 'PageTokenSecret'
  FROM
    `tsave_token_ig`
  UNION
  SELECT
    `tsave_token_twitter`.`screen_name` AS 'PageName',
    `tsave_token_twitter`.`user_id` AS 'PageID',
    'Twitter' AS 'Channel',
    `tsave_token_twitter`.`screen_name` AS 'AccountName',
    `tsave_token_twitter`.`user_id` AS 'AccountID',
    `tsave_token_twitter`.`oauth_token` AS 'PageToken',
    `tsave_token_twitter`.`oauth_token_secret` AS 'PageTokenSecret'
  FROM
    `tsave_token_twitter`) listtoken
WHERE
  (`listtoken`.`PageID` <> 'null');
